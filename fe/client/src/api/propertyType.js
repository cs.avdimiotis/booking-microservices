import { useQuery } from 'react-query';
import axios from 'axios';

const fetchByPropertyType = () => {
  return axios.get('http://localhost:4000/propertyType');

  // const { data, loading, error } = useFetch(
  //   `${process.env.REACT_APP_JS_API_BASE_URL}hotels/countByType`
  // );
};

const usePropertyTypes = () => {
  return useQuery('propertyTypes', fetchByPropertyType, {
    select: (initialData) => {
      const data = initialData?.data.map((item) => ({
        ...item,
        count: item.count.toLocaleString()
      }));
      return data;
    }
  });
};

export { usePropertyTypes };
