import { useQuery } from 'react-query';
import axios from 'axios';

const fetchLinks = () => {
  return axios.get('http://localhost:4000/links');
};

const useLinks = () => {
  return useQuery('links', fetchLinks, {
    select: (initialData) => {
      const data = initialData?.data.reduce(
        (acc, obj) => {
          const categoryIndex = parseInt(obj.category) - 1;

          if (!acc[categoryIndex]) {
            acc[categoryIndex] = [];
          }

          const { category, ...newLink } = obj;

          acc[categoryIndex].push(newLink);
          return acc;
        },
        [[], [], [], [], []]
      );

      return data;
    }
  });
};

export { useLinks };
