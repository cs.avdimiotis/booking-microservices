import { useQuery } from 'react-query';
import axios from 'axios';

const fetchFeaturedPositions = () => {
  return axios.get('http://localhost:4000/featured');

  // const { data, loading, error } = useFetch(  //TODO create a correct call towards BE
  //   '/hotels/countByCity?cities=berlin,madrid,london'
  // );
};

const useFeaturedPositions = () => {
  return useQuery('featured', fetchFeaturedPositions, {
    select: (initialData) => {
      const data = initialData?.data.map((item) => ({
        ...item,
        numOfProperties: item.numOfProperties.toLocaleString()
      }));
      return data;
    }
  });
};

const fetchFeaturedProperties = () => {
  return axios.get('http://localhost:4000/hotels?featured=true');
  // const { data, loading, error } = useFetch(
  //   `${process.env.REACT_APP_JS_API_BASE_URL}hotels?featured=true`
  // );
};

const useFeaturedProperties = () => {
  return useQuery('featuredHotels', fetchFeaturedProperties, {
    select: (initialData) => {
      const data = initialData?.data.map((item) => ({
        ...item,
        reviews: item.reviews.toLocaleString(),
        cheapestPrice: item.cheapestPrice.toLocaleString()
      }));
      return data;
    }
  });
};

export { useFeaturedPositions, useFeaturedProperties };
