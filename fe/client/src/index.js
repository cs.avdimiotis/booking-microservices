import './css_templates';
// import { StrictMode } from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import { CommonProviders, PopUpProvider } from './modules';
import { QueryClientProvider, QueryClient } from 'react-query';
import { ReactQueryDevtools } from 'react-query/devtools';

const queryClient = new QueryClient();

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  //<StrictMode>
  <QueryClientProvider client={queryClient}>
    <CommonProviders>
      <PopUpProvider>
        <App />
      </PopUpProvider>
    </CommonProviders>
    <ReactQueryDevtools initialIsOpen={false} position="bottom-right" />
  </QueryClientProvider>
  //</StrictMode>
);
