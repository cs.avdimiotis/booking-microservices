import axios from 'axios';
import { useState, memo } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAuth } from '../../modules';
import './login.css';

const Login = memo(() => {
  const [credentials, setCredentials] = useState({
    username: undefined,
    password: undefined
  });

  const { loading, error, startLogin, afterSuccessfulLogin, afterFailedLogin } =
    useAuth();

  const navigate = useNavigate();

  const handleChange = (e) => {
    setCredentials((prev) => ({ ...prev, [e.target.id]: e.target.value }));
  };

  const handleClick = async (e) => {
    e.preventDefault();
    startLogin();
    try {
      const res = await axios.post(
        `${process.env.REACT_APP_JS_API_BASE_URL}auth/login`,
        credentials
      );
      afterSuccessfulLogin(res.data.details);
      navigate('/');
    } catch (err) {
      afterFailedLogin(err.response.data);
    }
  };

  const handleSignUp = () => {
    navigate('/register');
  };

  return (
    <div className="login">
      <div className="lContainer">
        <input
          type="text"
          placeholder="username"
          id="username"
          onChange={handleChange}
          className="lInput"
        />
        <input
          type="password"
          placeholder="password"
          id="password"
          onChange={handleChange}
          className="lInput"
        />
        <button disabled={loading} onClick={handleClick} className="lButton">
          Login
        </button>
        {error && <span>Username or password is wrong. Please try again</span>}
        <span>
          First time?
          <a href="" onClick={handleSignUp}>
            Sign up
          </a>
        </span>
      </div>
    </div>
  );
});

export default Login;
