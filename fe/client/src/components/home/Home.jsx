import { memo } from 'react';
import {
  Featured,
  FeaturedProperties,
  Footer,
  Header,
  MailList,
  Navbar,
  PropertyList
} from '../../modules';

import './home.css';

const Home = memo(() => {
  console.log('Home rendered');
  return (
    <div>
      <Navbar />
      <Header />
      <div className="homeContainer">
        <Featured />
        <PropertyList />
        <FeaturedProperties />
        <MailList />
        <Footer />
      </div>
    </div>
  );
});

export default Home;
