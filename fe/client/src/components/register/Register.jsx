import axios from 'axios';
import { useState, memo } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAuth } from '../../modules';
import './register.css';

const Register = memo(() => {
  const [credentials, setCredentials] = useState({
    username: undefined,
    email: undefined,
    password: undefined
  });

  const { loading, afterSuccessfulLogin, afterFailedLogin, startLogin } =
    useAuth();

  const navigate = useNavigate();

  const handleChange = (e) => {
    setCredentials((prev) => ({ ...prev, [e.target.id]: e.target.value }));
  };

  const handleClick = async (e) => {
    e.preventDefault();
    startLogin();
    try {
      const res = await axios.post(
        `${process.env.REACT_APP_JS_API_BASE_URL}auth/register`,
        credentials
      );
      afterSuccessfulLogin(res.data.details);
      navigate('/');
    } catch (err) {
      afterFailedLogin(err.response.data);
    }
  };

  const handleLogin = () => {
    navigate('/login');
  };

  return (
    <div className="login">
      <div className="lContainer">
        <input
          type="text"
          placeholder="username"
          id="username"
          onChange={handleChange}
          className="lInput"
        />
        <input
          type="text"
          placeholder="email"
          id="email"
          onChange={handleChange}
          className="lInput"
        />
        <input
          type="password"
          placeholder="password"
          id="password"
          onChange={handleChange}
          className="lInput"
        />
        <button disabled={loading} onClick={handleClick} className="lButton">
          Register
        </button>
        <span>
          Already registered?
          <a href="" onClick={handleLogin}>
            Login here
          </a>
        </span>
      </div>
    </div>
  );
});

export default Register;
