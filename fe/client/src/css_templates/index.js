/**
 * Colors
 */
import './palette.css';

/**
 * Shapes
 */
import './shapes.css';
