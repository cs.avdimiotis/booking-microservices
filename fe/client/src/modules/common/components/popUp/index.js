import { PopUpProvider, usePopUp } from './context/PopUpContext';
import { typeOfWindows as popUps } from './core/typeOfWindows';
import PopUp from './PopUp';

// Constants
export { popUps };

// Hooks

// Contexts
export { PopUpProvider, usePopUp };
// Components
export { PopUp };
