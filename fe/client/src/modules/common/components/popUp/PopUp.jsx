import { memo, useRef, useEffect } from 'react';
import './popUp.css';
import { usePopUp } from './context/PopUpContext';

const PopUp = memo(function PopUp({ children, title, typeOfWindow }) {
  const { closePopup, popupsVisibilityState } = usePopUp();
  const popupRef = useRef(null);
  const popUpTitle = title || 'Oops an error happened';

  // Close if click outside
  useEffect(() => {
    const handleClickOutside = (event) => {
      if (popupRef.current && !popupRef.current.contains(event.target)) {
        closePopup(typeOfWindow);
      }
    };
    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [popupRef, typeOfWindow, closePopup]);

  // Close if click on close button
  const handleCloseClick = () => {
    closePopup(typeOfWindow);
  };

  return (
    <>
      {popupsVisibilityState[typeOfWindow] && (
        <>
          <div className="overlay"></div>
          <div className="popup" ref={popupRef}>
            <div
              className="popup-title"
              data-testid={`popup-title-${typeOfWindow}`}
            >
              {popUpTitle}
              <button
                onClick={handleCloseClick}
                className="popup-close-button"
                aria-label="Close"
                data-testid={`popup-close-button-${typeOfWindow}`}
              >
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                  <path d="M13.31 12l6.89-6.89a.93.93 0 1 0-1.31-1.31L12 10.69 5.11 3.8A.93.93 0 0 0 3.8 5.11L10.69 12 3.8 18.89a.93.93 0 0 0 1.31 1.31L12 13.31l6.89 6.89a.93.93 0 1 0 1.31-1.31z"></path>
                </svg>
              </button>
            </div>
            {children}
          </div>
        </>
      )}
    </>
  );
});

export default PopUp;
