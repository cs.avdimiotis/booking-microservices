import { createContext, useContext, useState, useMemo } from 'react';

const PopUpContext = createContext();

const PopUpProvider = ({ children }) => {
  const [popupsVisibilityState, setPopups] = useState({});

  const openPopup = (popupId) => {
    setPopups((prevState) => ({
      ...prevState,
      [popupId]: true
    }));
  };

  const closePopup = (popupId) => {
    setPopups((prevState) => ({
      ...prevState,
      [popupId]: false
    }));
  };

  const popUpValues = useMemo(() => {
    return { popupsVisibilityState };
  }, [popupsVisibilityState]);

  const popUpActions = useMemo(() => {
    return {
      openPopup,
      closePopup
    };
  }, [openPopup, closePopup]);

  return (
    <PopUpContext.Provider value={{ ...popUpValues, ...popUpActions }}>
      {children}
    </PopUpContext.Provider>
  );
};

const usePopUp = () => {
  return useContext(PopUpContext);
};

export { PopUpProvider, usePopUp };
