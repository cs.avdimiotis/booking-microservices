import { memo } from 'react';
import './primaryButton.css';

const PrimaryButton = memo(({ text, handler }) => {
  return (
    <button
      className="headerBtn"
      onClick={handler}
      data-testid={`button-${text}`}
    >
      {text}
    </button>
  );
});

export default PrimaryButton;
