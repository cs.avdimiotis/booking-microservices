import { createContext, useEffect, useReducer, useContext } from 'react';

const INITIAL_STATE = {
  user: JSON.parse(localStorage.getItem('user')) || null,
  loading: false,
  error: null
};

const AuthContext = createContext(INITIAL_STATE);

const AuthReducer = (state, action) => {
  switch (action.type) {
    case 'LOGIN_START':
      return {
        user: null,
        loading: true,
        error: null
      };
    case 'LOGIN_SUCCESS':
      return {
        user: action.payload,
        loading: false,
        error: null
      };
    case 'LOGIN_FAILURE':
      return {
        user: null,
        loading: false,
        error: action.payload
      };
    case 'LOGOUT':
      return {
        user: null,
        loading: false,
        error: null
      };
    default:
      return state;
  }
};

const useAuth = () => {
  const { state, dispatch } = useContext(AuthContext);

  return {
    user: state.user,
    loading: state.loading,
    error: state.error,
    startLogin: () => {
      dispatch({ type: 'LOGIN_START' });
    },
    afterSuccessfulLogin: (data) => {
      dispatch({ type: 'LOGIN_SUCCESS', payload: data });
    },
    afterFailedLogin: (data) => {
      dispatch({ type: 'LOGIN_FAILURE', payload: data });
    },
    afterLogout: () => {
      dispatch({ type: 'LOGOUT' });
    }
  };
};

const AuthContextProvider = ({ children }) => {
  const [state, dispatch] = useReducer(AuthReducer, INITIAL_STATE);

  useEffect(() => {
    localStorage.setItem('user', JSON.stringify(state.user));
  }, [state.user]);

  return (
    <AuthContext.Provider value={{ state, dispatch }}>
      {children}
    </AuthContext.Provider>
  );
};

export { useAuth, AuthContextProvider };
