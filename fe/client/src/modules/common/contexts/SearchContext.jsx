import {
  createContext,
  useReducer,
  useContext,
  useCallback,
  useMemo
} from 'react';

const INITIAL_STATE = {
  city: '',
  dates: [
    {
      startDate: new Date(),
      endDate: new Date()
    }
  ],
  options: {
    adult: 1,
    children: 0,
    room: 1
  }
};

const SearchReducer = (state, action) => {
  switch (action.type) {
    case 'NEW_SEARCH':
      return action.payload;
    case 'RESET_SEARCH':
      return INITIAL_STATE;
    case 'UPDATE CITY':
      return {
        ...state,
        city: action.city
      };
    case 'UPDATE DATES':
      return {
        ...state,
        dates: action.dates
      };
    default:
      return state;
  }
};

const SearchContext = createContext(INITIAL_STATE);

const SearchContextProvider = ({ children }) => {
  const [state, dispatch] = useReducer(SearchReducer, INITIAL_STATE);

  const providerValue = useMemo(() => ({ state, dispatch }), [state, dispatch]);

  return (
    <SearchContext.Provider value={providerValue}>
      {children}
    </SearchContext.Provider>
  );
};

const useSearch = () => {
  const { state, dispatch } = useContext(SearchContext);

  return {
    city: state.city,
    dates: state.dates,
    options: state.options,
    updateCity: useCallback(
      (city) => {
        dispatch({
          type: 'UPDATE CITY',
          city: city
        });
      },
      [dispatch]
    ),
    updateDates: useCallback(
      (dates) => {
        console.log('updateDates');
        dispatch({
          type: 'UPDATE DATES',
          dates: dates
        });
      },
      [dispatch]
    ),
    newSearch: useCallback(
      (city, dates, options) => {
        dispatch({
          type: 'NEW_SEARCH',
          payload: { city, dates, options }
        });
      },
      [dispatch]
    ),
    resetSearch: useCallback(() => {
      dispatch({
        type: 'RESET_SEARCH',
        payload: INITIAL_STATE
      });
    }, [dispatch])
  };
};

export { useSearch, SearchContextProvider };
