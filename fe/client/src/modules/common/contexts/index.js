import { AuthContextProvider, useAuth } from './AuthContext';
import { SearchContextProvider, useSearch } from './SearchContext';

const composeProviders =
  (...providers) =>
  ({ children }) => {
    return providers.reduceRight(
      (child, Provider) => <Provider>{child}</Provider>,
      children
    );
  };

const CommonProviders = composeProviders(
  AuthContextProvider,
  SearchContextProvider
);

// Hooks

// Contexts & Providers
export { useAuth, useSearch, CommonProviders };
// Components
