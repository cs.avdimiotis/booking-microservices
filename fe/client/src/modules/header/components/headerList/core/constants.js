import {
  faBed,
  faCar,
  faPlane,
  faTaxi
} from '@fortawesome/free-solid-svg-icons';
import { texts } from './texts';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export const choices = [
  {
    option: 'Stays',
    text: texts.stays, //TODO implement some translation logic in utils folder
    icon: <FontAwesomeIcon icon={faBed} data-cy="list-item-stays-icon" />
  },
  {
    option: 'Flights',
    text: texts.flights,
    icon: <FontAwesomeIcon icon={faPlane} data-cy="list-item-flights-icon" />
  },
  {
    option: 'Car rentals',
    text: texts.car_rentals,
    icon: <FontAwesomeIcon icon={faCar} data-cy="list-item-car-rentals-icon" />
  },
  {
    option: 'Attractions',
    text: texts.attractions,
    icon: <FontAwesomeIcon icon={faBed} data-cy="list-item-attractions-icon" />
  },
  {
    option: 'Airport taxis',
    text: texts.airport_taxis,
    icon: (
      <FontAwesomeIcon icon={faTaxi} data-cy="list-item-airport-taxis-icon" />
    )
  }
];
