export const texts = {
  stays: 'Stays',
  flights: 'Flights',
  car_rentals: 'Car rentals',
  attractions: 'Attractions',
  airport_taxis: 'Airport taxis'
};
