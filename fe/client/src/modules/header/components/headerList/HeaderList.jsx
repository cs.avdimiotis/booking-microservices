import './headerList.css';
import { useState, memo } from 'react';
import 'react-date-range/dist/styles.css';
import 'react-date-range/dist/theme/default.css';
import { choices } from './core/constants.js';

const HeaderList = memo(() => {
  const [activeChoice, setActiveChoice] = useState(choices[0]?.option || '');

  const handleHeaderList = (option) => {
    setActiveChoice(option);
  };
  return (
    <div className="headerList">
      {choices?.map((choice) => (
        <div
          key={choice.option}
          className={`headerListItem ${
            activeChoice !== choice.option ? 'inactive' : ''
          }`}
          onClick={() => handleHeaderList(choice.option)}
        >
          {choice.icon}
          <span data-testid={`list-item-${choice.option}`}>{choice.text}</span>
        </div>
      ))}
    </div>
  );
});

export default HeaderList;
