export const texts = {
  title: `A lifetime of discounts? It's Genius.`,
  description: `Get rewarded for your travels – unlock instant savings of 10% or more
    with a free Giotbooking account`
};
