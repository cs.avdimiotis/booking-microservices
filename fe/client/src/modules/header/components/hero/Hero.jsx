import './constants/texts';
import { texts } from './constants/texts';
import { memo } from 'react';
import './hero.css';

const Hero = memo(() => {
  return (
    <>
      <h1 className="heroTitle">{texts.title}</h1>
      <p className="heroDescription">{texts.description}</p>
    </>
  );
});

export default Hero;
