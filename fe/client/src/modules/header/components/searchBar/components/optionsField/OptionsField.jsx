import { faPerson } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './optionsField.css';
import { useState, useRef, memo } from 'react';
import { useOnClickOutside } from '../common/hooks';

const OptionsField = memo(({ value, setter }) => {
  console.log('OptionsField rendered');
  const ref = useRef();
  const [openOptions, setOpenOptions] = useState(false);

  useOnClickOutside(ref, () => {
    setOpenOptions(false);
  });

  const handleOption = (name, operation) => {
    setter((prev) => {
      return {
        ...prev,
        [name]: operation === 'i' ? value[name] + 1 : value[name] - 1
      };
    });
  };

  return (
    <div ref={ref}>
      <FontAwesomeIcon icon={faPerson} className="headerIcon" />
      <span
        onClick={() => setOpenOptions(!openOptions)}
        className="headerSearchText"
      >{`${value.adult} adult · ${value.children} children · ${value.room} room`}</span>
      {openOptions && (
        <div className="options">
          <div className="optionItem">
            <span className="optionText">Adult</span>
            <div className="optionCounter">
              <button
                disabled={value.adult <= 1}
                className="optionCounterButton"
                onClick={() => handleOption('adult', 'd')}
              >
                -
              </button>
              <span className="optionCounterNumber">{value.adult}</span>
              <button
                className="optionCounterButton"
                onClick={() => handleOption('adult', 'i')}
              >
                +
              </button>
            </div>
          </div>
          <div className="optionItem">
            <span className="optionText">Children</span>
            <div className="optionCounter">
              <button
                disabled={value.children <= 0}
                className="optionCounterButton"
                onClick={() => handleOption('children', 'd')}
              >
                -
              </button>
              <span className="optionCounterNumber">{value.children}</span>
              <button
                className="optionCounterButton"
                onClick={() => handleOption('children', 'i')}
              >
                +
              </button>
            </div>
          </div>
          <div className="optionItem">
            <span className="optionText">Room</span>
            <div className="optionCounter">
              <button
                disabled={value.room <= 1}
                className="optionCounterButton"
                onClick={() => handleOption('room', 'd')}
              >
                -
              </button>
              <span className="optionCounterNumber">{value.room}</span>
              <button
                className="optionCounterButton"
                onClick={() => handleOption('room', 'i')}
              >
                +
              </button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
});

export default OptionsField;
