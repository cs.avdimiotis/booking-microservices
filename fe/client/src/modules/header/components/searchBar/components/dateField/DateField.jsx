import { faCalendarDays } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { DateRange } from 'react-date-range';
import { useState, useRef, memo } from 'react';
import { useOnClickOutside } from '../common/hooks';
import { format } from 'date-fns';

import '../common/common.css';
import './dateField.css';
import 'react-date-range/dist/styles.css'; // main css file
import 'react-date-range/dist/theme/default.css'; // theme css file

const DateField = memo(({ value, setter }) => {
  console.log('DateField rendered');
  const ref = useRef();

  const [openDate, setOpenDate] = useState(false);

  useOnClickOutside(ref, () => {
    setOpenDate(false);
  });

  return (
    <div ref={ref}>
      <FontAwesomeIcon icon={faCalendarDays} className="headerIcon" />
      <span
        onClick={() => setOpenDate(!openDate)}
        className="headerSearchText"
        data-testid="choose-days"
      >{`${format(value[0]?.startDate, 'dd/MM/yyyy')} to ${format(
        value[0]?.endDate,
        'dd/MM/yyyy'
      )}`}</span>
      {openDate && (
        <DateRange
          editableDateInputs={true}
          onChange={(item) => setter([item.selection])}
          moveRangeOnFirstSelection={false}
          ranges={value}
          className="date"
          minDate={new Date()}
        />
      )}
    </div>
  );
});

export default DateField;
