import { memo } from 'react';
import { useNavigate } from 'react-router-dom';

import { PrimaryButton, useSearch } from '../../../../../common';

const SubmitButton = memo(() => {
  console.log('Submit button rendered');

  const { city, dates, options } = useSearch();

  const navigate = useNavigate();

  const handleSearch = () => {
    navigate('/hotels', { state: { destination: city, dates, options } });
  };

  return <PrimaryButton handler={handleSearch} text="Search" />;
});

export default SubmitButton;
