import { memo, useState } from 'react';
import { faBed } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useSearch } from '../../../../../common';
import '../common/common.css';

const DestinationField = memo(() => {
  // console.log('DestinationField rendered');

  const { city, updateCity } = useSearch();
  const [destination, setDestination] = useState(city || '');

  const handleDestination = (destination) => {
    setDestination(destination);
    updateCity(destination);
  };

  return (
    <>
      <FontAwesomeIcon icon={faBed} className="headerIcon" />
      <input
        type="text"
        value={destination}
        placeholder="Where are you going?"
        className="headerSearchInput"
        data-testid="searchfield"
        onChange={(e) => handleDestination(e.target.value)}
      />
    </>
  );
});

export default DestinationField;
