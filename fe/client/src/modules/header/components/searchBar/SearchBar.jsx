import { useState, memo, useCallback } from 'react';

import {
  DateField,
  OptionsField,
  DestinationField,
  SubmitButton
} from './components';

import './searchBar.css';

const SearchBar = memo(() => {
  console.log('Searchbar rendered');

  const [dates, setDates] = useState([
    {
      startDate: new Date(),
      endDate: new Date(),
      key: 'selection'
    }
  ]);
  const [options, setOptions] = useState({
    adult: 1,
    children: 0,
    room: 1
  });

  const handleDates = useCallback((value) => {
    setDates(value);
  }, []);
  const handleOptions = useCallback((value) => {
    setOptions(value);
  }, []);

  return (
    <div className="headerSearch">
      <div className="headerSearchItem">
        <DestinationField />
      </div>
      <div className="headerSearchItem">
        <DateField value={dates} setter={handleDates} />
      </div>
      <div className="headerSearchItem">
        <OptionsField value={options} setter={handleOptions} />
      </div>
      <div className="headerSearchItem">
        <SubmitButton />
      </div>
    </div>
  );
});

export default SearchBar;
