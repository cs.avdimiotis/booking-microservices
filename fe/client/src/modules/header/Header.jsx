import { memo } from 'react';
import { useNavigate } from 'react-router-dom';

import { PrimaryButton, useAuth } from '../common';
import { HeaderList, Hero, SearchBar } from './components';

import './header.css';

const Header = memo(({ type }) => {
  console.log('Header rendered');
  const navigate = useNavigate();
  const { user } = useAuth();

  const handleSignIn = () => {
    navigate('login');
  };

  return (
    <div className="header" data-testid="header-container">
      <div
        className={
          type === 'list' ? 'headerContainer listMode' : 'headerContainer'
        }
      >
        <HeaderList />
        {type !== 'list' && (
          <>
            <Hero />
            {!user && (
              <PrimaryButton handler={handleSignIn} text="Sign in / Register" />
            )}
            <SearchBar />
          </>
        )}
      </div>
    </div>
  );
});

export default Header;
