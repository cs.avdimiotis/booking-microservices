import './featured.css';
import { memo } from 'react';
import { useFeaturedPositions } from '../../api/featured';
import { Card, Skeleton } from '@mui/material';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';

const Featured = memo(() => {
  const { isLoading, data, isError, error } = useFeaturedPositions();

  if (isLoading) {
    return (
      <Grid container spacing={1} sx={{ maxWidth: '80%' }}>
        <Grid item xs={12} sm={12}>
          <Skeleton width={'60%'} sx={{ fontSize: '1.5rem' }} />
          <Skeleton width={'80%'} sx={{ fontSize: '1.1rem' }} />
        </Grid>
        {[0, 1, 2, 3, 4, 5].map((el) => (
          <Grid item xs={6} sm={2} key={el}>
            <Skeleton variant="rectangular" width={'100%'} height={118} />
            <Skeleton width={'60%'} />
            <Skeleton width={'80%'} />
            <Skeleton width={'60%'} />
          </Grid>
        ))}
      </Grid>
    );
  }

  if (isError) {
    return <h2>{error?.message}</h2>;
  }

  return (
    <>
      <Grid container spacing={1} sx={{ maxWidth: '80%' }}>
        <Grid item xs={12} sm={12}>
          <Typography
            variant="h5"
            component="div"
            fontWeight="bold"
            align="left"
          >
            Explore Greece
          </Typography>
          <Typography
            variant="body"
            component="div"
            color="grey.600"
            align="left"
          >
            These popular destinations have a lot to offer
          </Typography>
        </Grid>
        {data?.map((item, index) => (
          <Grid
            item
            xs={4}
            sm={2}
            sx={{ cursor: 'pointer' }}
            data-testid={`container-${item.name}`}
            key={item.name}
          >
            <Card sx={{ boxShadow: 'none', p: 0 }}>
              <CardMedia
                sx={{ height: 140, borderRadius: 2, m: 0 }}
                image={item.img}
                title="place-image"
                data-testid={`image-${item.name}`}
              />
              <CardContent sx={{ p: 1 }}>
                <Typography
                  variant="h6"
                  component="div"
                  fontWeight="bold"
                  data-testid={`location-${item.name}`}
                >
                  {item.name}
                </Typography>
                <Typography
                  variant="body2"
                  color="grey.600"
                  data-testid={`properties-${item.name}`}
                >
                  {item.numOfProperties} properties
                </Typography>
              </CardContent>
            </Card>
          </Grid>
        ))}
      </Grid>
    </>
  );
});

export default Featured;
