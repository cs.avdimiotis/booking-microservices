import { memo } from 'react';
import './mailList.css';

const MailList = memo(() => {
  return (
    <div className="mail">
      <h1 className="mailTitle">Save time, save money!</h1>
      <h5 className="mailTitle">It's Easy!</h5>
      <span className="mailDesc">
        Sign up and we'll send the best deals to you
      </span>
      <div className="mailInputContainer">
        <label htmlFor="email">Email</label>
        <input type="text" placeholder="Your Email" id="email" />
        <button>Subscribe</button>
      </div>
    </div>
  );
});

export default MailList;
