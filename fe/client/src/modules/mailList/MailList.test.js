import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import MailList from './MailList';

describe('Modules/mailList', () => {
  //Some url here of the JIRA Test case (if exists)
  test('Module is rendered', () => {
    render(<MailList />);
  });

  test('Header exists', () => {
    render(<MailList />);

    const header = screen.getByRole('heading', {
      name: 'Save time, save money!',
      level: 1
    });
    expect(header).toBeInTheDocument();

    const subheader = screen.getByRole('heading', {
      name: `It's Easy!`,
      level: 5
    });
    expect(subheader).toBeInTheDocument();
  });

  test('Email exists', () => {
    const { getByLabelText } = render(<MailList />);

    const el = getByLabelText('Email');
    expect(el).toBeInTheDocument();
  });
});
