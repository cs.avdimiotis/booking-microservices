import { memo } from 'react';
import './featuredProperties.css';
import { useFeaturedProperties } from '../../api/featured';
import {
  Skeleton,
  Card,
  CardContent,
  CardMedia,
  Typography,
  Grid,
  Box,
  Stack
} from '@mui/material';

const FeaturedProperties = memo(() => {
  const { isLoading, data, isError, error } = useFeaturedProperties();

  if (isLoading) {
    return (
      <Grid container spacing={1} sx={{ maxWidth: '80%' }}>
        <Grid item xs={12} sm={12}>
          <Skeleton width={'60%'} sx={{ fontSize: '1.5rem' }} />
        </Grid>
        {[0, 1, 2, 3, 4].map((el) => (
          <Grid item xs={2} sm={3} key={el}>
            <Skeleton variant="rectangular" width={'100%'} height={118} />
            <Skeleton width={'60%'} />
            <Skeleton width={'80%'} />
            <Skeleton width={'60%'} />
          </Grid>
        ))}
      </Grid>
    );
  }

  if (isError) {
    return <h2>{error.message}</h2>;
  }

  return (
    <>
      <Stack sx={{ maxWidth: '80%' }}>
        <Grid item xs={12} sm={12}>
          <Typography
            variant="h5"
            component="div"
            fontWeight="bold"
            align="left"
          >
            Homes guests love
          </Typography>
        </Grid>
        <Grid item xs={12} sm={12}>
          <Grid container spacing={1}>
            {data?.map((item, index) => (
              <Grid
                item
                xs={3}
                sm={3}
                sx={{ cursor: 'pointer', p: 0 }}
                data-testid={`container-${item.type}`}
                key={item.name}
              >
                <Card sx={{ height: '100%' }}>
                  <CardMedia
                    sx={{
                      height: 140,
                      maxWidth: '100%',
                      borderRadius: 2,
                      m: 0
                    }}
                    image={item.img}
                    title="place-image"
                    data-testid={`image-${item.name}`}
                  />
                  <CardContent
                    sx={{
                      p: 1,
                      '&:last-child': {
                        paddingBottom: 0
                      }
                    }}
                  >
                    <Typography
                      variant="body"
                      component="div"
                      fontWeight="bold"
                      fontSize="1.3vw;"
                      data-testid={`location-${item.name}`}
                    >
                      {item.name}
                    </Typography>
                    <Typography
                      variant="body2"
                      color="grey.600"
                      fontSize="0.95vw"
                      marginTop={'2px'}
                      component="div"
                      data-testid={`position-${item.name}`}
                    >
                      {item.area}, {item.country}, {item.city}
                    </Typography>
                    <Typography
                      variant="body2"
                      color="grey.600"
                      marginTop={'8px'}
                      component="div"
                      fontSize="10px"
                      data-testid={`price-${item.name}`}
                    >
                      <span className="fpRating">{item.rating}</span>{' '}
                      {item.critic} · {item.reviews} reviews
                    </Typography>
                    <Box
                      sx={{
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'end',
                        justifyContent: 'flex-end'
                      }}
                    >
                      <Typography
                        variant="body2"
                        color="grey.600"
                        fontSize="0.95vw"
                        textAlign={'right'}
                        component="div"
                        data-testid={`price-${item.name}`}
                      >
                        Starting from
                        <span className="fpPrice"> € {item.cheapestPrice}</span>
                      </Typography>
                    </Box>
                  </CardContent>
                </Card>
              </Grid>
            ))}
          </Grid>
        </Grid>
      </Stack>
    </>
  );
});

export default FeaturedProperties;
