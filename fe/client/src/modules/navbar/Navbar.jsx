import './navbar.css';
import { memo } from 'react';
import { Link, useNavigate } from 'react-router-dom';

import { useAuth } from '../common';
import { Language, Help, UserMenu } from './components';

const Navbar = memo(() => {
  const { user } = useAuth();
  const navigate = useNavigate();

  const handleRegisterOrLogin = (action) => {
    if (user) navigate('/');
    else {
      if (action === 'register') navigate('/register');
      else navigate('/login');
    }
  };

  return (
    <div className="navbar" data-testid="navigation-bar">
      <div className="navContainer">
        <Link to="/" style={{ color: 'inherit', textDecoration: 'none' }}>
          <span className="logo" data-testid="logo">
            GiotBooking
          </span>
        </Link>
        <div className="navItems">
          <Language />
          {user ? (
            <>
              <Help />
              <a
                className="property"
                href={`${process.env.REACT_APP_ADMIN_UI_URL}`}
              >
                List your property
              </a>
              <UserMenu />
            </>
          ) : (
            <>
              <button
                className="navButton"
                data-testid="register-button"
                onClick={() => handleRegisterOrLogin('register')}
              >
                Register
              </button>
              <button
                className="navButton"
                onClick={() => handleRegisterOrLogin('login')}
                data-testid="login-button"
              >
                Login
              </button>
            </>
          )}
        </div>
      </div>
    </div>
  );
});

export default Navbar;
