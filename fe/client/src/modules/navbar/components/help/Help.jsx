import HelpOutlineIcon from '@mui/icons-material/HelpOutline';
import { HelpSections } from './components';
import { PopUp, usePopUp, popUps } from '../../../common';

import '../common/common.css';
import './help.css';

function Help() {
  const { openPopup } = usePopUp();
  const typeOfWindow = popUps.help;

  const handlePopupClick = () => {
    openPopup(typeOfWindow);
  };

  return (
    <>
      <button className="navButton userMenuButton" onClick={handlePopupClick}>
        <HelpOutlineIcon fontSize="small" data-testid="help-icon" />
      </button>
      <PopUp title="How can we help?" typeOfWindow={typeOfWindow}>
        <HelpSections />
      </PopUp>
    </>
  );
}

export default Help;
