export const staticData = [
  {
    question: `When do I pay?`,
    answer: `Payment is taken at the time of booking your ticket by Booking.com on behalf of the attraction operator.`
  },
  {
    question: `Do I need to print my tickets?`,
    answer: `It depends on the attraction you book. Some accept both printed tickets and digital tickets shown on your phone, while other attractions can only accept paper tickets. For those ones, you'll need to print out your tickets in advance and bring them with you to the venue. You can check what kind of tickets are accepted on the attraction's page and in your confirmation email.`
  }
];
