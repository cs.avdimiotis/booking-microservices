import * as React from 'react';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { useFetch } from '../../../../../common';
import { staticData } from './core/constants';

function HelpSections() {
  const { data, loading, error } = useFetch(
    `${process.env.REACT_APP_JAVA_API_BASE_URL}faqs`
  );

  function template(dataToShow) {
    return (
      <>
        {dataToShow?.map((faq, index) => (
          <Accordion key={index}>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography>{faq.question}</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>{faq.answer}</Typography>
            </AccordionDetails>
          </Accordion>
        ))}
      </>
    );
  }

  return (
    <div>
      {loading ? <p>Please wait</p> : null}
      {data ? template(data) : null}
      {error ? template(staticData) : null}
    </div>
  );
}

export default HelpSections;
