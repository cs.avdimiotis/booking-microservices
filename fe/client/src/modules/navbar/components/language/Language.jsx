import '../common/common.css';
import { PopUp, popUps, usePopUp } from '../../../common';

function Language() {
  const { openPopup } = usePopUp();
  const typeOfWindow = popUps.language;

  const handlePopupClick = () => {
    openPopup(typeOfWindow);
  };

  return (
    <>
      <button
        className="navButton userMenuButton"
        data-testid="languagesection-languages-icon"
        onClick={handlePopupClick}
      >
        Languages
      </button>
      <PopUp title="Languages" typeOfWindow={typeOfWindow}>
        <div data-testid="languages-text">Languages</div>
      </PopUp>
    </>
  );
}

export default Language;
