import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Menu, MenuItem } from '@mui/material';
import { Row } from 'react-bootstrap';
import PermIdentityIcon from '@mui/icons-material/PermIdentity';
import WorkOutlineIcon from '@mui/icons-material/WorkOutline';
import GoogleIcon from '@mui/icons-material/Google';
import WalletIcon from '@mui/icons-material/Wallet';
import ReviewsIcon from '@mui/icons-material/Reviews';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import ExitToAppOutlinedIcon from '@mui/icons-material/ExitToAppOutlined';
import { useAuth } from '../../../common';

import './userMenu.css';
import '../common/common.css';

const UserMenu = () => {
  const [anchorElm, setAnchorElm] = useState(null);
  const [open, setOpen] = useState(false);
  const { user } = useAuth();
  const navigate = useNavigate();

  const handleClose = () => {
    // setAnchorElm(null);
    setOpen(false);
  };

  const handleSignOut = () => {
    localStorage.removeItem('user');
    navigate('/');
    window.location.reload(true);
  };

  const handleClick = (e) => {
    setAnchorElm(e.currentTarget);
    setOpen(!open);
  };

  const handleRewardsAndWallet = () => {
    window.location.replace(`${process.env.REACT_APP_USERMENU_UI_URL}`);
  };

  return (
    <>
      <button
        className="navButton userMenuButton"
        onClick={handleClick}
        data-testid="user-menu-button"
      >
        <Row className="username" data-testid="user-menu-username">
          {user.username}
        </Row>
        <Row className="level" data-testid="user-menu-userlevel">
          Genius Level 2
        </Row>
      </button>
      <Menu
        anchorEl={anchorElm}
        open={open}
        onClose={handleClose}
        data-testid="usermenu"
      >
        <MenuItem onClick={handleClose} data-testid="manage-account-option">
          <PermIdentityIcon
            fontSize="small"
            data-testid="manage-account-icon"
          />
          <span className="menuItem" data-testid="manage-account-text">
            Manage account
          </span>
        </MenuItem>
        <MenuItem onClick={handleClose} data-testid="booking-trips-option">
          <WorkOutlineIcon fontSize="small" data-testid="booking-trips-icon" />
          <span className="menuItem" data-testid="booking-trips-text">
            Booking & Trips
          </span>
        </MenuItem>
        <MenuItem onClick={handleClose} data-testid="genius-loyalty-programme">
          <GoogleIcon
            fontSize="small"
            data-testid="genius-loyalty-programme-icon"
          />
          <span className="menuItem">Genius loyalty programme</span>
        </MenuItem>
        <MenuItem
          onClick={handleRewardsAndWallet}
          data-testid="rewards-and-wallet"
        >
          <WalletIcon fontSize="small" data-testid="rewards-and-wallet-icon" />
          <span className="menuItem">Rewards & Wallet</span>
        </MenuItem>
        <MenuItem onClick={handleClose} data-testid="reviews">
          <ReviewsIcon fontSize="small" data-testid="reviews-icon" />
          <span className="menuItem">Reviews</span>
        </MenuItem>
        <MenuItem onClick={handleClose} data-testid="saved-options">
          <FavoriteBorderIcon
            fontSize="small"
            data-testid="saved-options-icon"
          />
          <span className="menuItem">Saved</span>
        </MenuItem>
        <MenuItem onClick={handleSignOut} data-testid="sign-out">
          <ExitToAppOutlinedIcon fontSize="small" data-testid="sign-out-icon" />
          <span className="menuItem">Sign out</span>
        </MenuItem>
      </Menu>
    </>
  );
};

export default UserMenu;
