import { memo } from 'react';
import './propertyList.css';
import { usePropertyTypes } from '../../api/propertyType';
import {
  Skeleton,
  Card,
  CardContent,
  CardMedia,
  Typography,
  Grid
} from '@mui/material';
const PropertyList = memo(() => {
  const { isLoading, data, isError, error } = usePropertyTypes();

  if (isLoading) {
    return (
      <Grid container spacing={1} sx={{ maxWidth: '80%' }}>
        <Grid item xs={12} sm={12}>
          <Skeleton width={'60%'} sx={{ fontSize: '1.5rem' }} />
        </Grid>
        {[0, 1, 2, 3, 4, 5].map((el) => (
          <Grid item xs={6} sm={2} key={el}>
            <Skeleton variant="rectangular" width={'100%'} height={118} />
            <Skeleton width={'60%'} />
            <Skeleton width={'80%'} />
            <Skeleton width={'60%'} />
          </Grid>
        ))}
      </Grid>
    );
  }

  if (isError) {
    return <h2>{error.message}</h2>;
  }

  return (
    <>
      <Grid container spacing={1} sx={{ maxWidth: '80%' }}>
        <Grid item xs={12} sm={12}>
          <Typography
            variant="h5"
            component="div"
            fontWeight="bold"
            align="left"
          >
            Browse by property type
          </Typography>
        </Grid>
        {data?.map((item, index) => (
          <Grid
            item
            xs={4}
            sm={2}
            sx={{ cursor: 'pointer' }}
            data-testid={`container-${item.type}`}
            key={item.name}
          >
            <Card sx={{ boxShadow: 'none', p: 0 }}>
              <CardMedia
                sx={{ height: 140, borderRadius: 2, m: 0 }}
                image={item.img}
                title="place-image"
                data-testid={`image-${item.type}`}
              />
              <CardContent sx={{ p: 1 }}>
                <Typography
                  variant="h6"
                  component="div"
                  fontWeight="bold"
                  data-testid={`location-${item.type}`}
                >
                  {item.type}
                </Typography>
                <Typography
                  variant="body2"
                  color="grey.600"
                  data-testid={`properties-${item.type}`}
                >
                  {item.count} {item.type}
                </Typography>
              </CardContent>
            </Card>
          </Grid>
        ))}
      </Grid>
    </>
  );
});

export default PropertyList;
