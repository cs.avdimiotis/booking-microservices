//Copywrite message

//Imports
import { memo } from 'react';
import './footer.css';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import { useLinks } from '../../api/links';

const Footer = memo(() => {
  const { isLoading, data, isError, error } = useLinks();

  if (isLoading) {
    return <h2> Loading...</h2>;
  }

  if (isError) {
    return <h2>{error?.message}</h2>;
  }

  return (
    <Grid container spacing={2}>
      {/* In mobile view this should be omitted */}
      <Grid item xs={6} sm={1}></Grid>
      {data?.map((myList, linkIndex) => (
        <Grid item xs={6} sm={2} key={linkIndex}>
          {myList?.map((link, index) => (
            <Box
              key={index}
              sx={{
                mb: '10px',
                '&:hover': {
                  cursor: 'pointer'
                }
              }}
            >
              <Link
                key={index}
                href={link.url}
                variant="body2"
                underline="none"
                color="grey.800"
              >
                {link.text}
              </Link>
            </Box>
          ))}
        </Grid>
      ))}
    </Grid>
    // <div className="footer">
    //   <div className="fLists">
    //     <ul className="fList">
    //       <li className="fListItem">Countries</li>
    //       <li className="fListItem">Regions</li>
    //       <li className="fListItem">Cities</li>
    //       <li className="fListItem">Districts</li>
    //       <li className="fListItem">Airports</li>
    //       <li className="fListItem">Hotels</li>
    //     </ul>
    //     <ul className="fList">
    //       <li className="fListItem">Homes </li>
    //       <li className="fListItem">Apartments </li>
    //       <li className="fListItem">Resorts </li>
    //       <li className="fListItem">Villas</li>
    //       <li className="fListItem">Hostels</li>
    //       <li className="fListItem">Guest houses</li>
    //     </ul>
    //     <ul className="fList">
    //       <li className="fListItem">Unique places to stay </li>
    //       <li className="fListItem">Reviews</li>
    //       <li className="fListItem">Unpacked: Travel articles </li>
    //       <li className="fListItem">Travel communities </li>
    //       <li className="fListItem">Seasonal and holiday deals </li>
    //     </ul>
    //     <ul className="fList">
    //       <li className="fListItem">Car rental </li>
    //       <li className="fListItem">Flight Finder</li>
    //       <li className="fListItem">Restaurant reservations </li>
    //       <li className="fListItem">Travel Agents </li>
    //     </ul>
    //     <ul className="fList">
    //       <li className="fListItem">Curtomer Service</li>
    //       <li className="fListItem">Partner Help</li>
    //       <li className="fListItem">Careers</li>
    //       <li className="fListItem">Sustainability</li>
    //       <li className="fListItem">Press center</li>
    //       <li className="fListItem">Safety Resource Center</li>
    //       <li className="fListItem">Investor relations</li>
    //       <li className="fListItem">Terms & conditions</li>
    //     </ul>
    //   </div>
    //   <div className="fText">Copyright © 2023 GiotBooking.</div>
    // </div>
  );
});

export default Footer;
