import express, { Application, Request, Response, NextFunction } from "express";
import dotenv from "dotenv";
import * as http from "http";
import { RoutesConfig } from "./common/routes.config";
import { CustomError } from "./common/error.types";
import ErrorHandler from "./middleware/errorHandler.exceptions";
import { connect } from "./utils/db.utils";
import { logger } from "./utils/logger";
import { BooksRoutes } from "./routes/books.routes.config";
import { UserRoutes } from "./routes/users.routes.config";

const app: Application = express();
dotenv.config();

const server: http.Server = http.createServer(app);
const port = process.env.PORT || 5000;

const routes: Array<RoutesConfig> = [];

app.use(express.json());

// Routes
routes.push(new BooksRoutes(app));
routes.push(new UserRoutes(app));

// Error handling
app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
  // 1. Log the error or send it to a 3rd party error monitoring software
  //TODO Implement winston here
  next(err);
});

app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
  // 2. Send an email/message to somewhere /something
  //TODO implement node-email smtp or twilio here
  next(err);
});

app.use(
  (
    err: Error | CustomError,
    req: Request,
    res: Response,
    next: NextFunction
  ) => {
    // 3. Handle the error here
    ErrorHandler.handleError(err, res);
  }
);

server.listen(port, () => {
  connect();
  routes.forEach((route: RoutesConfig) => {
    logger.info(`Routes configured for ${route.getName()}`);
  });
  logger.info(`Server is running at http://localhost:${port}`);
});
