import { Request, Response, NextFunction } from "express";
import User, { IUser } from "../models/user.model";
import {
  createUserService,
  getUserService,
  updateUserService,
  deleteUserService,
} from "../services/users.services.config";
import {
  UserRequestBody,
  UserRequestParameters,
  UserRequestBodyAndParameters,
} from "../common/reqInterfaces.config";
import bcrypt from "bcryptjs";
import { CustomError, HttpCode } from "../common/error.types";

class UserController {
  public async createUser(
    req: UserRequestBody<IUser>,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    try {
      //TODO data-validation here
      const salt = bcrypt.genSaltSync(10);
      const hash = bcrypt.hashSync(req.body.password, salt);

      const user: IUser = {
        username: req.body.username,
        email: req.body.email,
        password: hash,
        author_pseudonym: req.body.author_pseudonym,
      };

      const token: string | null = await createUserService(user);

      res
        .setHeader("authorization", token)
        .status(HttpCode.OK)
        .send({ success: true, message: "User created", token: token });
    } catch (error: CustomError | any) {
      next(error);
    }
  }

  public async getUser(
    req: UserRequestParameters<{ id: string }>,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    try {
      //TODO data-validation here
      const id: string = req.params.id;
      const user: IUser | null = await getUserService(id);
      res
        .status(HttpCode.OK)
        .send({ success: true, message: "User found", user: user });
    } catch (error: CustomError | any) {
      let returnedError = {
        status: error.httpCode || HttpCode.BAD_REQUEST,
        success: false,
        message: error.description || "An error happened",
      };
      res
        .status(returnedError.status)
        .send({ success: false, message: returnedError.message });
    }
  }

  public async updateUser(
    req: UserRequestBodyAndParameters<{ id: string }, IUser>,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    try {
      //TODO data-validation here
      const user: IUser = {
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
        author_pseudonym: req.body.author_pseudonym,
      };

      const id: string = req.params.id;
      await updateUserService(id, user);
      res.status(HttpCode.OK).send({ success: true, message: "User updated" });
    } catch (error: CustomError | any) {
      let returnedError = {
        status: error.httpCode || HttpCode.BAD_REQUEST,
        success: false,
        message: error.description || "An error happened",
      };
      res
        .status(returnedError.status)
        .send({ success: false, message: returnedError.message });
    }
  }

  public async deleteUser(
    req: UserRequestParameters<{ id: string }>,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    try {
      const id: string = req.params.id;
      await deleteUserService(id);
      res.status(HttpCode.OK).send({ success: true, message: "User deleted" });
    } catch (error: CustomError | any) {
      let returnedError = {
        status: error.httpCode || HttpCode.BAD_REQUEST,
        success: false,
        message: error.description || "An error happened",
      };
      res
        .status(returnedError.status)
        .send({ success: false, message: returnedError.message });
    }
  }
}

export default new UserController();
