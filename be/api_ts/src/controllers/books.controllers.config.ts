import { Request, Response, NextFunction } from "express";
import { IBook } from "../models/books.model";
import {
  createBookService,
  getAllBooksService,
  getBookService,
  deleteBookService,
} from "../services/books.services.config";
import {
  UserRequestBody,
  UserRequestParameters,
  UserRequestBodyAndParameters,
} from "../common/reqInterfaces.config";
import { CustomError, HttpCode } from "../common/error.types";

class BookController {
  public async getAllBooks(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    try {
      //TODO data-validation here
      const books: IBook[] | null = await getAllBooksService();
      res
        .status(HttpCode.OK)
        .send({ success: true, message: "Books found", books: books });
    } catch (error: CustomError | any) {
      next(error);
    }
  }

  public async createBook(
    req: UserRequestBody<IBook>,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    try {
      //TODO data-validation here

      if (res.locals.jwtPayload.username === "_Darth Vader_")
        throw new CustomError({
          httpCode: HttpCode.UNAUTHORIZED,
          description: "Sorry Darthy, Wookies don't love you",
        });

      const book: IBook = {
        title: req.body.title,
        author: req.body.author,
        description: req.body.description,
        coverImage: req.body.coverImage,
        price: req.body.price,
      };

      await createBookService(book);
      res.status(HttpCode.OK).send({ success: true, message: "Book created" });
    } catch (error: CustomError | any) {
      next(error);
    }
  }

  public async getBook(
    req: UserRequestParameters<{ id: string }>,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    try {
      //TODO data-validation here
      const id: string = req.params.id;
      const book: IBook | null = await getBookService(id);
      res
        .status(HttpCode.OK)
        .send({ success: true, message: "Book found", book: book });
    } catch (error: CustomError | any) {
      next(error);
    }
  }

  public async deleteBook(
    req: UserRequestParameters<{ id: string }>,
    res: Response,
    next: NextFunction
  ): Promise<void> {
    try {
      const bookId: string = req.params.id;
      const book = await getBookService(bookId);
      if (book.author === res.locals.jwtPayload.userId) {
        await deleteBookService(bookId);
        res
          .status(HttpCode.OK)
          .send({ success: true, message: "Book deleted" });
      } else {
        throw new CustomError({
          httpCode: HttpCode.UNAUTHORIZED,
          description: "You are not allowed to delete other person's book",
        });
      }
    } catch (error: CustomError | any) {
      next(error);
    }
  }
}

export default new BookController();
