import mongoose from "mongoose";
import dotenv from "dotenv";
import { logger } from "../utils/logger";
dotenv.config();

const db: string = process.env.MONGO || "";

export const connect = async () => {
  try {
    await mongoose.connect(db);
    logger.info("Connected to DB");
  } catch (error) {
    throw error;
  }
};

mongoose.set("strictQuery", false);

mongoose.connection.on("diconnected", () => {
  logger.info("Mongo DB disconnected");
});
