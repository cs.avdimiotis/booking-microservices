import { Query } from "express-serve-static-core";

export interface UserRequestBody<T> extends Express.Request {
  body: T;
}

export interface UserRequestParameters<T> extends Express.Request {
  params: T;
}

export interface UserRequestBodyAndParameters<U, T> extends Express.Request {
  params: U;
  body: T;
}

export interface UserLocals<T> extends Express.Response {
  locals: T;
}
