export enum HttpCode {
  OK = 200,
  NO_CONTENT = 204,
  BAD_REQUEST = 400,
  UNAUTHORIZED = 401,
  NOT_FOUND = 404,
  INTERNAL_SERVER_ERROR = 500
}

interface Customizable {
  name?: string;
  httpCode: HttpCode;
  description: string;
  isOperational?: boolean; // <-- Some categorization
}

export class CustomError extends Error {
  public name: string;
  public httpCode: HttpCode;
  public isOperational: boolean = true;

  constructor(args: Customizable) {
    super(args.description);

    // Object.setPrototypeOf(this, new.target.prototype);

    this.name = args.name || 'Unknown Error';
    this.httpCode = args.httpCode || HttpCode.INTERNAL_SERVER_ERROR;

    if (args.isOperational !== undefined)
      this.isOperational = args.isOperational;

    // Error.captureStackTrace(this);
  }
}
