import { Response } from "express";
import { logger } from "../utils/logger";
import { CustomError, HttpCode } from "../common/error.types";

class ErrorHandler {
  private isTrustedError(error: Error): boolean {
    // Trusted errors are only custom errors with operational status

    if (error instanceof CustomError) return error.isOperational;
    return false;
  }

  public handleError(error: Error | CustomError, response: Response): void {
    if (this.isTrustedError(error) && response) {
      this.handleTrustedError(error as CustomError, response);
    } else {
      this.handleCriticalError(error, response);
    }
  }

  private handleTrustedError(error: CustomError, response: Response): void {
    response
      .status(error.httpCode)
      .json({ success: false, message: error.message });
  }

  private handleCriticalError(
    error: Error | CustomError,

    response: Response
  ): void {
    if (response) {
      response
        .status(HttpCode.INTERNAL_SERVER_ERROR)
        .json({ success: false, message: "Internal server error" });
    }

    logger.info("Application encountered a critical error. Exiting");
    process.exit(1);
  }
}

export default new ErrorHandler();
