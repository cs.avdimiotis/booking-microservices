import { Request, Response, NextFunction } from "express";
import * as jwt from "jsonwebtoken";
import { HttpCode } from "../common/error.types";
import dotenv from "dotenv";
dotenv.config();

export const checkJwt = (req: Request, res: Response, next: NextFunction) => {
  //Get the jwt token from the head
  const token = <string>req.headers["authorization"];
  let jwtPayload;
  let secret: string = process.env.JWT_SECRET || "";

  //Try to validate the token and get data
  try {
    jwtPayload = <any>jwt.verify(token, secret);
    res.locals.jwtPayload = jwtPayload;
  } catch (error) {
    //If token is not valid, respond with 401 (unauthorized)
    res
      .status(HttpCode.UNAUTHORIZED)
      .send({ success: false, message: "You are not authenticated" });
    return;
  }
  //Call the next middleware or controller
  next();
};
