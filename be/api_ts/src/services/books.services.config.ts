import { CustomError, HttpCode } from "../common/error.types";
import Book, { IBook } from "../models/books.model";

export const getAllBooks = async (req: Request, res: Response) => {
  const allBooks = await Book.find();
  if (allBooks === null)
    throw new CustomError({
      httpCode: HttpCode.NOT_FOUND,
      description: "Books not found",
    });
  return allBooks;
};

export const createBookService = async (book: IBook): Promise<void> => {
  const newBook = new Book(book);
  await newBook.save();
};

export const getAllBooksService = async (): Promise<IBook[] | null> => {
  const book: IBook[] | null = await Book.find();
  if (!book.length)
    throw new CustomError({
      httpCode: HttpCode.NOT_FOUND,
      description: "No book found",
    });
  return book;
};

export const getBookService = async (id: string): Promise<IBook> => {
  const book: IBook | null = await Book.findById(id);
  if (book === null)
    throw new CustomError({
      httpCode: HttpCode.NOT_FOUND,
      description: "Book not found",
    });
  return book;
};

export const deleteBookService = async (id: string): Promise<void> => {
  await Book.findByIdAndDelete(id);
};
