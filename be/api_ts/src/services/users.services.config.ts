import User, { IUser } from "../models/user.model";
import * as jwt from "jsonwebtoken";
import dotenv from "dotenv";
import { CustomError, HttpCode } from "../common/error.types";
dotenv.config();

export const getUserService = async (id: string): Promise<IUser> => {
  const user: IUser | null = await User.findById(id);
  if (user === null)
    throw new CustomError({
      httpCode: HttpCode.NOT_FOUND,
      description: "User not found",
    });
  return user;
};

export const createUserService = async (inserted: IUser): Promise<string> => {
  const newUser = new User(inserted);

  const user = await newUser.save();
  const secret: string = process.env.JWT_SECRET || "";

  const token: string = jwt.sign(
    { userId: user._id, username: user.username },
    secret
  );
  return token;
};

export const updateUserService = async (
  id: string,
  user: IUser
): Promise<void> => {
  await User.findByIdAndUpdate(
    id,
    {
      $set: user,
    },
    { new: true }
  );
};

export const deleteUserService = async (id: string): Promise<void> => {
  await User.findByIdAndDelete(id);
};
