import { RoutesConfig } from "../common/routes.config";
import express, { Application, Request, Response, NextFunction } from "express";
import BookController from "../controllers/books.controllers.config";
import Book, { IBook } from "../models/books.model";
import {
  UserRequestBody,
  UserRequestParameters,
  UserLocals,
} from "../common/reqInterfaces.config";
import { checkJwt } from "../middleware/auth.middleware";

export class BooksRoutes extends RoutesConfig {
  constructor(app: Application) {
    super(app, "BooksRoutes");
  }

  configureRoutes(prefix: string): express.Application {
    this.app.post(
      `${prefix}/book`,
      checkJwt,
      (req: UserRequestBody<IBook>, res: Response, next: NextFunction) => {
        BookController.createBook(req, res, next);
      }
    );

    this.app.get(
      `${prefix}/all_books`,
      (req: Request, res: Response, next: NextFunction) => {
        BookController.getAllBooks(req, res, next);
      }
    );

    this.app.get(
      `${prefix}/book/:id`,
      (
        req: UserRequestParameters<{ id: string }>,
        res: Response,
        next: NextFunction
      ) => {
        BookController.getBook(req, res, next);
      }
    );

    this.app.delete(
      `${prefix}/book/:id`,
      checkJwt,
      (
        req: UserRequestParameters<{ id: string }>,
        res: Response,
        next: NextFunction
      ) => {
        BookController.deleteBook(req, res, next);
      }
    );

    return this.app;
  }
}
