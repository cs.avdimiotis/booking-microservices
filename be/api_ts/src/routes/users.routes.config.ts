import { RoutesConfig } from "../common/routes.config";
import express, { Application, Request, Response, NextFunction } from "express";
import {
  UserRequestBody,
  UserRequestParameters,
  UserRequestBodyAndParameters,
} from "../common/reqInterfaces.config";
import { IUser } from "../models/user.model";
import UserController from "../controllers/users.controller.config";
import { checkJwt } from "../middleware/auth.middleware";

export class UserRoutes extends RoutesConfig {
  constructor(app: Application) {
    super(app, "UserRoutes");
  }

  configureRoutes(prefix: string): express.Application {
    this.app.post(
      `${prefix}/user`,
      (req: UserRequestBody<IUser>, res: Response, next: NextFunction) => {
        UserController.createUser(req, res, next);
      }
    );

    this.app.get(
      `${prefix}/user/:id`,
      checkJwt,
      (
        req: UserRequestParameters<{ id: string }>,
        res: Response,
        next: NextFunction
      ) => {
        UserController.getUser(req, res, next);
      }
    );

    this.app.put(
      `${prefix}/user/:id`,
      checkJwt,
      (
        req: UserRequestBodyAndParameters<{ id: string }, IUser>,
        res: Response,
        next: NextFunction
      ) => {
        UserController.updateUser(req, res, next);
      }
    );

    this.app.delete(
      `${prefix}/user/:id`,
      checkJwt,
      (
        req: UserRequestParameters<{ id: string }>,
        res: Response,
        next: NextFunction
      ) => {
        UserController.deleteUser(req, res, next);
      }
    );

    return this.app;
  }
}
