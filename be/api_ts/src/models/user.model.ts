import mongoose, { Schema } from 'mongoose';

export interface IUser {
  username: string,
  email: string,
  password: string,
  author_pseudonym: string
};

export const UserSchema: Schema = new mongoose.Schema(
  {
    username: {
      type: String,
      required: true,
      unique: true
    },
    email: {
      type: String,
      required: true,
      unique: true
    },
    password: {
      type: String,
      required: true
    },
    author_pseudonym: {
      type: String,
      required: true
    },
  }
);

const User = mongoose.model<IUser>('User', UserSchema);
export default User;