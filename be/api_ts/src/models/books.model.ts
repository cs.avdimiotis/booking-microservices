import mongoose from "mongoose";
import { IUser, UserSchema } from "./user.model";
import { Schema } from "mongoose";

export interface IBook {
  title: string;
  author: IUser;
  description: string;
  coverImage: string;
  price: number;
}

export const BooksSchema: Schema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  author: {
    type: Schema.Types.ObjectId,
    ref: "User",
    required: true,
    default: false,
  },
  description: {
    type: String,
    required: false,
    default: false,
  },
  coverImage: {
    type: String,
    default: false,
  },
  price: {
    type: Number,
    required: true,
    default: false,
  },
});

const Book = mongoose.model<IBook>("Book", BooksSchema);
export default Book;
