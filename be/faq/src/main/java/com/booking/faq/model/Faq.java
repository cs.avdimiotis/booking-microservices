package com.booking.faq.model;

import jakarta.persistence.*;

@Entity
@Table
public class Faq {
    @Id
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "faq_sequence"
    )
    @SequenceGenerator(
            name = "faq_sequence",
            sequenceName = "faq_sequence",
            allocationSize = 1
    )
    private Long id;
    private String question;

    @Column(length = 1500)
    private String answer;
    public Faq() {
    }

    public Faq(Long id,
               String question,
               String answer) {
        this.id = id;
        this.question = question;
        this.answer = answer;
    }

    public Faq(String question,
               String answer) {
        this.question = question;
        this.answer = answer;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    @Override
    public String toString() {
        return "FAQ{" +
                "id=" + id +
                ", question='" + question + '\'' +
                ", answer='" + answer +
                '}';
    }
}
