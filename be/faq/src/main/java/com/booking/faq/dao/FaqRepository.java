package com.booking.faq.dao;

import com.booking.faq.model.Faq;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FaqRepository
        extends JpaRepository<Faq, Long> {

//    @Query("SELECT s FROM Faq s WHERE s.email = ?1")
//    Optional<Faq> findStudentByEmail(String email);
//
//    @Query("UPDATE Student s set s.name = ?1, s.email = ?2 where s.id = ?3")
//    void setStudentInfoById(String name, String email, Long studentId);
}
