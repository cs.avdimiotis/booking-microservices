package com.booking.faq.services;

import com.booking.faq.model.Faq;
import com.booking.faq.dao.FaqRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FaqService {

    private final FaqRepository faqRepository;

    @Autowired
    public FaqService(FaqRepository faqRepository) {
        this.faqRepository = faqRepository;
    }

    public List<Faq> getFaqs(){
        return faqRepository.findAll();
    }
}
