package com.booking.faq.controllers;

import com.booking.faq.model.Faq;
import com.booking.faq.services.FaqService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api/v1/faqs")
public class FaqController {

    private final FaqService faqService;

    @Autowired
    public FaqController(FaqService faqService) {
        this.faqService = faqService;
    }

    @GetMapping
    @CrossOrigin(origins = "http://localhost:3000")
    public List<Faq> getFaqs(){
        return faqService.getFaqs();
    }

//    @PostMapping
//    public void registerNewStudent(@RequestBody Faq student){
//        faqService.addNewStudent(student);
//    }
//
//    @DeleteMapping(path="{studentId}")
//    public void deleteStudent(@PathVariable("studentId") Long studentId){
//        faqService.deleteStudent(studentId);
//    }
//
//    @PutMapping(path="{studentId}")
//    public void updateStudent(@PathVariable("studentId") Long studentId,
//                              @RequestParam(required = false) String email,
//                              @RequestParam(required = false) String name){
//        faqService.updateStudent(studentId, email, name);
//    }
}
