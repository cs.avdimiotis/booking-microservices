package com.booking.faq.config;

import com.booking.faq.dao.FaqRepository;
import com.booking.faq.model.Faq;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class FaqConfig {

    @Bean
    CommandLineRunner commandLineRunner(FaqRepository faqRepository){
        return args -> {
            Faq question1 = new Faq(1L,
                    "How could Coronavirus (COVID-19) affect my visit?",
                    "As a result of Coronavirus, some attractions may be temporarily closed, have limited availability, or have other restrictions in place to help maintain physical distancing.\n" +
                            "These measures are being updated regularly, so it's best to check the attraction provider’s website directly for the latest information. You can also find links to official guidance in each country here.\n" +
                            "If you need to cancel an upcoming attraction booking, you can follow the cancellation link in your confirmation email, or contact us for help using the form below.");
            Faq question2 = new Faq(2L,
                    "How do I book a ticket?",
                    "First, pick the attraction that you would like to visit by clicking on its name or image. Then check the availability for the date/times you want to visit.\n" +
                            "If your desired date/time is available, you can then select the amount and type of tickets that you want. After this step, you are able to pay for the tickets by entering your card details or using a card you have stored on Booking.com.\n" +
                            "Once payment is complete, you will receive a confirmation email. Please note that separate emails will be sent for each transaction. Some attractions may require you to print tickets out or pick up physical tickets from a ticket collection point. This information is also shown on the attraction's page before you book.");
            faqRepository.saveAll(List.of(question1, question2));
        };
    }
}


//Commands needed
//CREATE DATABASE faq;
//GRANT ALL PRIVILEGES ON DATABASE "faqs" TO bob;
//CREATE SEQUENCE faq_sequence START 1;