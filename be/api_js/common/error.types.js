export const HttpCode = {
  OK: 200,
  NO_CONTENT: 204,
  BAD_REQUEST: 400,
  UNAUTHORIZED: 401,
  UNAUTHENTICATED: 403,
  NOT_FOUND: 404,
  INTERNAL_SERVER_ERROR: 500
};

export class BookingError extends Error {
  logFile;
  logMethod;
  logMessage;
  logStack;

  returnedMessage;
  returnedHttpCode;

  constructor(logFile, logMethod, logMessage, logStack) {
    super();

    this.logFile = logFile || '';
    this.logMethod = logMethod || '';
    this.logMessage = logMessage || '';
    this.logStack = logStack || '';

    this.returnedMessage = 'Oops an error happened!';
    this.returnedHttpCode = HttpCode.INTERNAL_SERVER_ERROR;
  }
}
