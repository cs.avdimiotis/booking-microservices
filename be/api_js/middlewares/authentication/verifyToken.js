import { HttpCode, BookingError } from '../../common/error.types.js';
import { tokenVerification } from '../../utils/tokenization/tokenization.js';

export { verifyUser, verifyAdmin };

const verifyToken = (req, res, next) => {
  const token = req.cookies.access_token;
  if (!token) {
    let newError = new BookingError(
      'verifyToken.js',
      'verifyToken',
      'No token found during a login attempt',
      null
    );

    newError.returnedHttpCode = HttpCode.UNAUTHORIZED;
    newError.returnedMessage = 'Unauthorized access denied';
    return next(newError);
  }

  try {
    const user = tokenVerification(token);
    return user;
  } catch (error) {
    let newError = new BookingError(
      'verifyToken.js',
      'verifyToken',
      error?.message,
      error?.stack
    );

    newError.returnedHttpCode = HttpCode.UNAUTHORIZED;
    newError.returnedMessage = 'Unauthorized access denied';
    next(newError);
  }
};

const verifyUser = (req, res, next) => {
  try {
    const { id } = req.params;
    const user = verifyToken(req, res, next);
    if (user.id === id || user.isAdmin) return next();

    throw new Error('Unauthorized access tried from user: ' + id);
  } catch (error) {
    let newError = new BookingError(
      'verifyToken.js',
      'verifyUser',
      error?.message,
      error?.stack
    );
    newError.returnedHttpCode = HttpCode.UNAUTHORIZED;
    newError.returnedMessage = 'Unauthorized access denied';
    next(newError);
  }
};

const verifyAdmin = (req, res, next) => {
  try {
    const user = verifyToken(req, res, next);
    if (user.isAdmin) return next();

    throw new Error('User tried to enter as admin ' + user.id);
  } catch (error) {
    let newError = new BookingError(
      'verifyToken.js',
      'verifyAdmin',
      error?.message,
      error?.stack
    );
    newError.returnedHttpCode = HttpCode.UNAUTHORIZED;
    newError.returnedMessage = 'Unauthorized access denied';
    next(newError);
  }
};
