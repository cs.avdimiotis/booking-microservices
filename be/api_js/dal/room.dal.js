import Room from '../models/Room.model.js';

export {
  createRoom,
  updateRoom,
  updateRoomAvailability,
  deleteRoom,
  findRoom,
  findRooms
};

const createRoom = async (room) => {
  return await Room.create(room);
};

const updateRoom = async (id, room) => {
  return await Room.findByIdAndUpdate(id, {
    $set: room
  });
};

const updateRoomAvailability = async (id, dates) => {
  return await Room.updateOne(
    { 'roomNumbers._id': id },
    {
      $push: {
        'roomNumbers.$.unavailableDates': dates
      }
    }
  );
};

const deleteRoom = async (id) => {
  return await Room.findByIdAndDelete(id);
};

const findRoom = async (id) => {
  return await Room.findById(id);
};

const findRooms = async () => {
  return await Room.find();
};
