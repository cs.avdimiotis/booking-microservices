import User from '../models/User.model.js';

export {
  createUser,
  findUserByUsername,
  findUserById,
  updateUser,
  deleteUser,
  findAllUsers
};

const createUser = async (user) => {
  return await User.create(user);
};

const findUserByUsername = async (username) => {
  return await User.findOne({ username: username });
};

const findUserById = async (id) => {
  return await User.findById(id);
};

const findAllUsers = async () => {
  return await User.find();
};

const updateUser = async (id, username, password, email, isAdmin) => {
  return await User.findByIdAndUpdate(
    id,
    {
      $set: { username, password, email, isAdmin }
    },
    { new: true }
  );
};

const deleteUser = async (id) => {
  return await User.findByIdAndDelete(id);
};
