import Hotel from '../models/Hotel.model.js';

export {
  createHotel,
  updateHotel,
  addHotelRooms,
  deleteHotel,
  deleteHotelRooms,
  findHotelById,
  findNumberOfHotelsByCity,
  findNumberOfHotelsByType,
  findLimitedFeaturedHotelsByPrice
};

const createHotel = async (hotel) => {
  return await Hotel.create(hotel);
};

const updateHotel = async (id, hotel) => {
  return await Hotel.findByIdAndUpdate(
    id,
    {
      $set: hotel
    },
    { new: true }
  );
};

const addHotelRooms = async (id, roomId) => {
  return await Hotel.findByIdAndUpdate(id, {
    $push: { rooms: roomId }
  });
};

const deleteHotelRooms = async (id, roomId) => {
  return await Hotel.findByIdAndUpdate(id, {
    $pull: { rooms: roomId }
  });
};

const deleteHotel = async (id) => {
  return await Hotel.findByIdAndDelete(id);
};

const findHotelById = async (id) => {
  return await Hotel.findById(id);
};

const findNumberOfHotelsByCity = async (city) => {
  return await Hotel.countDocuments({ city: city });
};

const findNumberOfHotelsByType = async (type) => {
  return await Hotel.countDocuments({ type: type });
};

const findLimitedFeaturedHotelsByPrice = async (featured, min, max, limit) => {
  return await Hotel.find({
    featured,
    cheapestPrice: { $gt: min | 1, $lt: max || 999 }
  }).limit(limit);
};
