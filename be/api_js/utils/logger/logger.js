import winston from 'winston';

function createLogger() {
  // Create a logger instance
  const logger = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    transports: [
      new winston.transports.Console(),
      new winston.transports.File({
        filename: './utils/logger/error.log',
        level: 'error'
      }),
      new winston.transports.File({
        filename: './utils/logger/combined.log'
      })
    ]
  });

  return logger;
}

export default createLogger();
