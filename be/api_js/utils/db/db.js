import mongoose from 'mongoose';
import dotenv from 'dotenv';
import logger from '../logger/logger.js';

dotenv.config();

export { connect, disconnected, close };

async function connect() {
  try {
    mongoose.set('strictQuery', false);
    await mongoose.connect(process.env.MONGO, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    });
  } catch (error) {
    logger.error({
      file: 'db.js',
      method: 'connect',
      message: error?.message,
      stack: error?.stack
    });
    throw new Error('Error connecting to MongoDB');
  }
}

function disconnected() {
  return mongoose.connection.on('disconnected', () => {
    logger.info('Disconnected from MongoDB');
  });
}

function close() {
  return mongoose.connection.close().then(() => {
    logger.info('Disconnected from MongoDB');
  });
}
