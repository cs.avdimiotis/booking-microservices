import bcrypt from 'bcryptjs';

export { hashPassword, comparePassword };

const digits = 10;

const hashPassword = (password) => {
  const salt = bcrypt.genSaltSync(digits);
  const hash = bcrypt.hashSync(password, salt);

  return hash;
};

const comparePassword = async (insertedPassword, storedPassword) => {
  return await bcrypt.compare(insertedPassword, storedPassword);
};
