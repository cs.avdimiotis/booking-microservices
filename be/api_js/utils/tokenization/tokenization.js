import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';
dotenv.config();

export { tokenSigning, tokenVerification };

const tokenSigning = (id, isAdmin) => {
  return jwt.sign({ id: id, isAdmin: isAdmin }, process.env.JWT);
};

const tokenVerification = (token) => {
  return jwt.verify(token, process.env.JWT);
};
