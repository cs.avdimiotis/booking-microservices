import express from 'express';
import {
  createHotelController,
  deleteHotelController,
  getHotelController,
  getHotelsController,
  updateHotelController,
  countByCityController,
  countByTypeController,
  getHotelRoomsController
} from '../controllers/hotel.controllers.js';
import { verifyAdmin } from '../middlewares/authentication/verifyToken.js';

const router = express.Router();

router.post('/', verifyAdmin, createHotelController);
router.put('/:id', verifyAdmin, updateHotelController);
router.delete('/:id', verifyAdmin, deleteHotelController);
router.get('/find/:id', getHotelController);
router.get('/', getHotelsController);
router.get('/countByCity', countByCityController);
router.get('/countByType', countByTypeController);
router.get('/room/:id', getHotelRoomsController);

export default router;
