import express from 'express';
import {
  createRoomController,
  deleteRoomController,
  getRoomController,
  getRoomsController,
  updateRoomController,
  updateRoomAvailabilityController
} from '../controllers/room.controllers.js';
import { verifyAdmin } from '../middlewares/authentication/verifyToken.js';

const router = express.Router();

router.post('/:hotelId', verifyAdmin, createRoomController);
router.put('/:id', verifyAdmin, updateRoomController);
router.put('/availability/:id', updateRoomAvailabilityController);
router.delete('/:id/:hotelId', verifyAdmin, deleteRoomController);
router.get('/:id', getRoomController);
router.get('/', getRoomsController);

export default router;
