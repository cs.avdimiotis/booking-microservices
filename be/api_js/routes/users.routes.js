import express from 'express';

import {
  updateUserController,
  deleteUserController,
  getUserController,
  getUsersController
} from '../controllers/user.controllers.js';
import {
  verifyUser,
  verifyAdmin
} from '../middlewares/authentication/verifyToken.js';

const router = express.Router();
router.put(
  '/:id',
  verifyUser,
  /* TODO data validator, caching etc */ updateUserController
);
router.delete('/:id', verifyUser, deleteUserController);
router.get('/:id', verifyUser, getUserController);
router.get('/', verifyAdmin, getUsersController);

export default router;
