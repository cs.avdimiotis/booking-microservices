import express from 'express';
import {
  loginController,
  registerController
} from '../controllers/auth.controllers.js';

const router = express.Router();

router.post('/register', /* TODO data validator */ registerController);
router.post('/login', loginController);

export default router;
