import { HttpCode, BookingError } from '../common/error.types.js';
import {
  updateUserService,
  deleteUserService,
  getUserService,
  getUsersService
} from '../services/user.services.js';

export {
  updateUserController,
  deleteUserController,
  getUserController,
  getUsersController
};

const updateUserController = async (req, res, next) => {
  try {
    const { id } = req.params;
    const { username, password, email, isAdmin } = req.body;

    await updateUserService(id, username, password, email, isAdmin);

    res.status(HttpCode.OK).send('User updated');
  } catch (error) {
    let newError;
    if (error instanceof BookingError) newError = error;
    else
      newError = new BookingError(
        'user.controller.js',
        'updateUserController',
        error?.message,
        error?.stack
      );

    newError.returnedHttpCode = HttpCode.NOT_FOUND;
    newError.returnedMessage = `User wasn't updated`;
    next(newError);
  }
};

const deleteUserController = async (req, res, next) => {
  try {
    const { id } = req.params;
    await deleteUserService(id);
    res.status(HttpCode.OK).json('User has deleted');
  } catch (error) {
    let newError;
    if (error instanceof BookingError) newError = error;
    else
      newError = new BookingError(
        'user.controller.js',
        'deleteUserController',
        error?.message,
        error?.stack
      );

    newError.returnedHttpCode = HttpCode.NOT_FOUND;
    newError.returnedMessage = `User wasn't deleted`;
    next(newError);
  }
};

const getUserController = async (req, res, next) => {
  try {
    const { id } = req.params;
    const user = await getUserService(id);
    res.status(HttpCode.OK).json(user);
  } catch (error) {
    let newError;
    if (error instanceof BookingError) newError = error;
    else
      newError = new BookingError(
        'user.controller.js',
        'getUserController',
        error?.message,
        error?.stack
      );

    newError.returnedHttpCode = HttpCode.NOT_FOUND;
    newError.returnedMessage = `User wasn't found`;
    next(newError);
  }
};

const getUsersController = async (req, res, next) => {
  try {
    const users = await getUsersService();
    res.status(HttpCode.OK).json(users);
  } catch (error) {
    let newError;
    if (error instanceof BookingError) newError = error;
    else
      newError = new BookingError(
        'user.controller.js',
        'getUsersController',
        error?.message,
        error?.stack
      );

    newError.returnedHttpCode = HttpCode.NOT_FOUND;
    newError.returnedMessage = `User weren't found`;
    next(newError);
  }
};
