import { HttpCode, BookingError } from '../common/error.types.js';
import {
  createRoomService,
  updateRoomService,
  updateRoomAvailabilityService,
  deleteRoomService,
  getRoomService,
  getRoomsService
} from '../services/room.services.js';

export {
  createRoomController,
  updateRoomController,
  updateRoomAvailabilityController,
  deleteRoomController,
  getRoomController,
  getRoomsController
};

const createRoomController = async (req, res, next) => {
  try {
    const { hotelId } = req.params;
    const { title, price, maxPeople, desc, roomNumbers } = req.body;

    await createRoomService(
      hotelId,
      title,
      price,
      maxPeople,
      desc,
      roomNumbers
    );

    res.status(HttpCode.OK).send('Room saved successfully');
  } catch (error) {
    let newError;
    if (error instanceof BookingError) newError = error;
    else
      newError = new BookingError(
        'room.controller.js',
        'createRoomController',
        error?.message,
        error?.stack
      );

    newError.returnedHttpCode = HttpCode.NOT_FOUND;
    newError.returnedMessage = `Room wasn't created`;
    next(newError);
  }
};

const updateRoomController = async (req, res, next) => {
  try {
    const { id } = req.params;
    const { title, price, maxPeople, desc, roomNumbers } = req.body;

    await updateRoomService(id, title, price, maxPeople, desc, roomNumbers);
    res.status(HttpCode.OK).send('Room updated successfully');
  } catch (error) {
    let newError;
    if (error instanceof BookingError) newError = error;
    else
      newError = new BookingError(
        'room.controller.js',
        'updateRoomController',
        error?.message,
        error?.stack
      );

    newError.returnedHttpCode = HttpCode.NOT_FOUND;
    newError.returnedMessage = `Room wasn't updated`;
    next(newError);
  }
};

const updateRoomAvailabilityController = async (req, res, next) => {
  try {
    const { id } = req.params;
    const { dates } = req.body;

    await updateRoomAvailabilityService(id, dates);
    res.status(HttpCode.OK).json('Room status has been updated.');
  } catch (error) {
    let newError;
    if (error instanceof BookingError) newError = error;
    else
      newError = new BookingError(
        'room.controller.js',
        'updateRoomAvailabilityController',
        error?.message,
        error?.stack
      );

    newError.returnedHttpCode = HttpCode.NOT_FOUND;
    newError.returnedMessage = `Room's availability wasn't updated`;
    next(newError);
  }
};

const deleteRoomController = async (req, res, next) => {
  try {
    const { hotelId, id } = req.params;

    await deleteRoomService(hotelId, id);
    res.status(HttpCode.OK).json('Room has been deleted');
  } catch (erroe) {
    let newError;
    if (error instanceof BookingError) newError = error;
    else
      newError = new BookingError(
        'room.controller.js',
        'deleteRoomController',
        error?.message,
        error?.stack
      );

    newError.returnedHttpCode = HttpCode.NOT_FOUND;
    newError.returnedMessage = `Room wasn't deleted`;
    next(newError);
  }
};

const getRoomController = async (req, res, next) => {
  try {
    const { id } = req.params;
    const room = await getRoomService(id);
    res.status(HttpCode.OK).json(room);
  } catch (error) {
    let newError;
    if (error instanceof BookingError) newError = error;
    else
      newError = new BookingError(
        'room.controller.js',
        'getRoomController',
        error?.message,
        error?.stack
      );

    newError.returnedHttpCode = HttpCode.NOT_FOUND;
    newError.returnedMessage = `Room wasn't found`;
    next(newError);
  }
};

const getRoomsController = async (req, res, next) => {
  try {
    const rooms = await getRoomsService();
    res.status(HttpCode.OK).json(rooms);
  } catch (error) {
    let newError;
    if (error instanceof BookingError) newError = error;
    else
      newError = new BookingError(
        'room.controller.js',
        'getRoomsController',
        error?.message,
        error?.stack
      );

    newError.returnedHttpCode = HttpCode.NOT_FOUND;
    newError.returnedMessage = `Rooms weren't found`;
    next(newError);
  }
};
