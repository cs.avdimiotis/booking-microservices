import { HttpCode, BookingError } from '../common/error.types.js';
import { registerService, loginService } from '../services/auth.services.js';

export { registerController, loginController };

const registerController = async (req, res, next) => {
  try {
    const { username, email, password } = req.body;

    await registerService(username, email, password);

    const response = await loginService(username, password);
    res
      .cookie('access_token', response.token, {
        httpOnly: true
      })
      .status(HttpCode.OK)
      .send({ details: response.details, isAdmin: response.isAdmin });
  } catch (error) {
    let newError;
    if (error instanceof BookingError) newError = error;
    else
      newError = new BookingError(
        'auth.controller.js',
        'registerController',
        error?.message,
        error?.stack
      );

    newError.returnedHttpCode = HttpCode.UNAUTHORIZED;
    newError.returnedMessage = `Unauthorized access denied`;
    next(newError);
  }
};

const loginController = async (req, res, next) => {
  try {
    const { username, password } = req.body;

    const response = await loginService(username, password);
    res
      .cookie('access_token', response.token, {
        httpOnly: true
      })
      .status(HttpCode.OK)
      .send({ details: response.details, isAdmin: response.isAdmin });
  } catch (error) {
    let newError;
    if (error instanceof BookingError) newError = error;
    else
      newError = new BookingError(
        'auth.controller.js',
        'loginController',
        error?.message,
        error?.stack
      );

    newError.returnedHttpCode = HttpCode.UNAUTHORIZED;
    newError.returnedMessage = `An error happened! Please try to login again`;
    next(newError);
  }
};
