import { BookingError, HttpCode } from '../common/error.types.js';
import {
  createHotelService,
  updateHotelService,
  deleteHotelService,
  getHotelService,
  getHotelsService,
  countByCityService,
  countByTypeService,
  getHotelRoomsService
} from '../services/hotel.services.js';

export {
  createHotelController,
  updateHotelController,
  deleteHotelController,
  getHotelController,
  getHotelsController,
  countByCityController,
  countByTypeController,
  getHotelRoomsController
};

const createHotelController = async (req, res, next) => {
  try {
    //TODO data validation here
    const {
      name,
      type,
      city,
      address,
      distance,
      photos,
      title,
      desc,
      rating,
      rooms,
      cheapestPrice,
      featured
    } = req.body;

    const hotel = {
      name,
      type,
      city,
      address,
      distance,
      photos,
      title,
      desc,
      rating,
      rooms,
      cheapestPrice,
      featured
    };
    await createHotelService(hotel);

    res.status(HttpCode.OK).send('Hotel saved successfully');
  } catch (error) {
    let newError;
    if (error instanceof BookingError) newError = error;
    else
      newError = new BookingError(
        'hotel.controller.js',
        'createHotelController',
        error?.message,
        error?.stack
      );

    newError.returnedHttpCode = HttpCode.NOT_FOUND;
    newError.returnedMessage = `Hotel wasn't created`;
    next(newError);
  }
};

const updateHotelController = async (req, res, next) => {
  try {
    const { id } = req.params;
    const {
      name,
      type,
      city,
      address,
      distance,
      photos,
      title,
      desc,
      rating,
      rooms,
      cheapestPrice,
      featured
    } = req.body;

    const hotel = {
      name,
      type,
      city,
      address,
      distance,
      photos,
      title,
      desc,
      rating,
      rooms,
      cheapestPrice,
      featured
    };
    await updateHotelService(hotel);

    res.status(HttpCode.OK).send('Hotel updated successfully');
  } catch (error) {
    let newError;
    if (error instanceof BookingError) newError = error;
    else
      newError = new BookingError(
        'hotel.controller.js',
        'updateHotelController',
        error?.message,
        error?.stack
      );

    newError.returnedHttpCode = HttpCode.NOT_FOUND;
    newError.returnedMessage = `Hotel wasn't updated`;
    next(newError);
  }
};

const deleteHotelController = async (req, res, next) => {
  try {
    const { id } = req.params;
    await deleteHotelService(id);
    res.status(HttpCode.OK).json('Hotel has deleted');
  } catch (error) {
    let newError;
    if (error instanceof BookingError) newError = error;
    else
      newError = new BookingError(
        'hotel.controller.js',
        'deleteHotelController',
        error?.message,
        error?.stack
      );

    newError.returnedHttpCode = HttpCode.NOT_FOUND;
    newError.returnedMessage = `Hotel wasn't deleted`;
    next(newError);
  }
};

const getHotelController = async (req, res, next) => {
  try {
    const { id } = req.params;
    const hotel = await getHotelService(id);
    res.status(HttpCode.OK).json(hotel);
  } catch (error) {
    let newError;
    if (error instanceof BookingError) newError = error;
    else
      newError = new BookingError(
        'hotel.controller.js',
        'getHotelController',
        error?.message,
        error?.stack
      );

    newError.returnedHttpCode = HttpCode.NOT_FOUND;
    newError.returnedMessage = `Hotel wasn't found`;
    next(newError);
  }
};

const getHotelsController = async (req, res, next) => {
  try {
    const { min, max, featured, limit } = req.query;
    const hotels = await getHotelsService(min, max, featured, limit);
    res.status(HttpCode.OK).json(hotels);
  } catch (error) {
    let newError;
    if (error instanceof BookingError) newError = error;
    else
      newError = new BookingError(
        'hotel.controller.js',
        'getHotelsController',
        error?.message,
        error?.stack
      );

    newError.returnedHttpCode = HttpCode.NOT_FOUND;
    newError.returnedMessage = `Hotels weren't found`;
    next(newError);
  }
};

const countByCityController = async (req, res, next) => {
  try {
    const citiesArray = req.query.cities;
    const list = await countByCityService(citiesArray);
    res.status(HttpCode.OK).json(list);
  } catch (error) {
    let newError;
    if (error instanceof BookingError) newError = error;
    else
      newError = new BookingError(
        'hotel.controller.js',
        'countByCityController',
        error?.message,
        error?.stack
      );

    newError.returnedHttpCode = HttpCode.NOT_FOUND;
    newError.returnedMessage = `Hotels weren't found`;
    next(newError);
  }
};

const countByTypeController = async (req, res, next) => {
  try {
    const listByType = await countByTypeService();
    res.status(HttpCode.OK).json(listByType);
  } catch (error) {
    let newError;
    if (error instanceof BookingError) newError = error;
    else
      newError = new BookingError(
        'hotel.controller.js',
        'countByTypeController',
        error?.message,
        error?.stack
      );

    newError.returnedHttpCode = HttpCode.NOT_FOUND;
    newError.returnedMessage = `Hotels weren't found`;
    next(newError);
  }
};

const getHotelRoomsController = async (req, res, next) => {
  try {
    const { id } = req.params;
    const list = await getHotelRoomsService(id);
    res.status(HttpCode.OK).json(list);
  } catch (error) {
    let newError;
    if (error instanceof BookingError) newError = error;
    else
      newError = new BookingError(
        'hotel.controller.js',
        'getHotelRoomsController',
        error?.message,
        error?.stack
      );

    newError.returnedHttpCode = HttpCode.NOT_FOUND;
    newError.returnedMessage = `Hotels weren't found`;
    next(newError);
  }
};
