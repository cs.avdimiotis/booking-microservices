import express from 'express';
import dotenv from 'dotenv';
import logger from './utils/logger/logger.js';
import { connect, disconnected, close } from './utils/db/db.js';
import authRoute from './routes/auth.routes.js';
import usersRoute from './routes/users.routes.js';
import hotelsRoute from './routes/hotels.routes.js';
import roomsRoute from './routes/rooms.routes.js';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import { HttpCode } from './common/error.types.js';

//TODO swagger documentation
//TODO configure bundler to replace relative paths, check for build

const app = express();
app.use(cors());
dotenv.config({ path: `.env.development.local`, override: true });

const port = process.env.PORT || 5000;

app.use(async (req, res, next) => {
  try {
    await connect();
    next();
  } catch (error) {
    console.error('Error connecting to MongoDB:', error);
    next(error);
  }
});

app.use(cookieParser());
app.use(express.json());
//TODO configure here in case of accepting XMLs

app.use('/api/v1/auth', authRoute);
app.use('/api/v1/users', usersRoute);
app.use('/api/v1/hotels', hotelsRoute);
app.use('/api/v1/rooms', roomsRoute);

app.use((req, res, next) => {
  try {
    close();
    next();
  } catch (error) {
    console.error('Error disconnecting from MongoDB:', error);
    res.status(HttpCode.INTERNAL_SERVER_ERROR).send('Internal Server Error');
  }
});

app.use((error, req, res, next) => {
  // 1. Log the error or send it to a 3rd party error monitoring software
  logger.error({
    file: error?.logFile || 'No filed found',
    method: error?.logMethod || 'No method files',
    message: error?.logMessage || error?.message,
    stack: error?.logStack || error?.stack
  });
  next(error);
});

app.use((error, req, res, next) => {
  // 2. Send an email/message to somewhere /something
  //TODO implement node-email smtp or twilio here //alerting
  next(error);
});

//Error handler
app.use((error, req, res, next) => {
  const errorStatus = error?.returnedHttpCode || HttpCode.INTERNAL_SERVER_ERROR;
  const errorMessage = error?.returnedMessage || 'Oops an error happened!';

  res.status(errorStatus).json({
    status: errorStatus,
    message: errorMessage
  });
});

app.listen(port, () => {
  connect();
  logger.info('Connected to backend!');
});
