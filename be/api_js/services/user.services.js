import {
  updateUser,
  deleteUser,
  findUserById,
  findAllUsers
} from '../dal/user.dal.js';
import { BookingError } from '../common/error.types.js';

export {
  updateUserService,
  deleteUserService,
  getUserService,
  getUsersService
};

const updateUserService = async (id, username, password, email, isAdmin) => {
  try {
    await updateUser(id, username, password, email, isAdmin);
    return;
  } catch (error) {
    throw new BookingError(
      'user.services.js',
      'updateUserService',
      error?.message,
      error?.stack
    );
  }
};

const deleteUserService = async (id) => {
  try {
    await deleteUser(id);
    return;
  } catch (error) {
    throw new BookingError(
      'user.services.js',
      'deleteUserService',
      error?.message,
      error?.stack
    );
  }
};

const getUserService = async (id) => {
  try {
    const user = await findUserById(id);
    return user;
  } catch (error) {
    throw new BookingError(
      'user.services.js',
      'getUserService',
      error?.message,
      error?.stack
    );
  }
};

const getUsersService = async () => {
  try {
    const users = await findAllUsers();
    return users;
  } catch (error) {
    throw new BookingError(
      'user.services.js',
      'getUsersService',
      error?.message,
      error?.stack
    );
  }
};
