import { findRoom } from '../dal/room.dal.js';
import {
  createHotel,
  updateHotel,
  deleteHotel,
  findHotelById,
  findNumberOfHotelsByCity,
  findNumberOfHotelsByType,
  findLimitedFeaturedHotelsByPrice
} from '../dal/hotel.dal.js';
import { BookingError } from '../common/error.types.js';

export {
  createHotelService,
  updateHotelService,
  deleteHotelService,
  getHotelService,
  getHotelsService,
  countByCityService,
  countByTypeService,
  getHotelRoomsService
};

//TODO fix services in case of no results
const createHotelService = async (hotel) => {
  try {
    await createHotel(hotel);
    return;
  } catch (error) {
    throw new BookingError(
      'hotel.services.js',
      'createHotelService',
      error?.message,
      error?.stack
    );
  }
};

const updateHotelService = async (hotel) => {
  try {
    await updateHotel(id, hotel);

    return;
  } catch (error) {
    throw new BookingError(
      'hotel.services.js',
      'updateHotelService',
      error?.message,
      error?.stack
    );
  }
};

const deleteHotelService = async (id) => {
  try {
    await deleteHotel(id);
    return;
  } catch (error) {
    throw new BookingError(
      'hotel.services.js',
      'deleteHotelService',
      error?.message,
      error?.stack
    );
  }
};

const getHotelService = async (id) => {
  try {
    const hotel = findHotelById(id);
    if (!hotel) throw new Error('Hotel not found');
    return hotel;
  } catch (error) {
    throw new BookingError(
      'hotel.services.js',
      'getHotelService',
      error?.message,
      error?.stack
    );
  }
};

const getHotelsService = async (min, max, featured, limit) => {
  try {
    const hotels = await findLimitedFeaturedHotelsByPrice(
      featured,
      min,
      max,
      limit
    );
    return hotels;
  } catch (error) {
    throw new BookingError(
      'hotel.services.js',
      'getHotelsService',
      error?.message,
      error?.stack
    );
  }
};

const countByCityService = async (citiesArray) => {
  try {
    const cities = citiesArray.split(',');
    const list = await Promise.all(
      cities.map(async (city) => {
        return await findNumberOfHotelsByCity(city);
      })
    );
    return list;
  } catch (error) {
    throw new BookingError(
      'hotel.services.js',
      'countByCityService',
      error?.message,
      error?.stack
    );
  }
};

const countByTypeService = async () => {
  try {
    const hotelCount = await findNumberOfHotelsByType('Hotel');
    const apartmentCount = await findNumberOfHotelsByType('apartment');
    const resortCount = await findNumberOfHotelsByType('resort');
    const villaCount = await findNumberOfHotelsByType('villa');
    const cabinCount = await findNumberOfHotelsByType('cabin');
    return [
      { type: 'Hotel', count: hotelCount },
      { type: 'apartments', count: apartmentCount },
      { type: 'resorts', count: resortCount },
      { type: 'villas', count: villaCount },
      { type: 'cabins', count: cabinCount }
    ];
  } catch (error) {
    throw new BookingError(
      'hotel.services.js',
      'countByTypeService',
      error?.message,
      error?.stack
    );
  }
};

const getHotelRoomsService = async (id) => {
  try {
    const hotel = await findHotelById(id);
    const list = await Promise.all(
      hotel.rooms.map(async (room) => {
        return await findRoom(room);
      })
    );
    return list;
  } catch (error) {
    throw new BookingError(
      'hotel.services.js',
      'getHotelRoomsService',
      error?.message,
      error?.stack
    );
  }
};
