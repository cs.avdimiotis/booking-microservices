import { createUser, findUserByUsername } from '../dal/user.dal.js';
import { hashPassword, comparePassword } from '../utils/hashing/hashing.js';
import { tokenSigning } from '../utils/tokenization/tokenization.js';
import { BookingError } from '../common/error.types.js';

export { registerService, loginService };

const registerService = async (username, email, password) => {
  try {
    const hash = hashPassword(password);

    const newUser = {
      username: username,
      email: email,
      password: hash,
      isAdmin: false
    };
    await createUser(newUser);

    return;
  } catch (error) {
    throw new BookingError(
      'auth.services.js',
      'registerService',
      error?.message,
      error?.stack
    );
  }
};

const loginService = async (username, insertedPassword) => {
  try {
    const user = await findUserByUsername(username);
    if (!user)
      throw new BookingError(
        'auth.services.js',
        'loginService',
        'User not found',
        error?.stack
      );

    const isPasswordCorrect = await comparePassword(
      insertedPassword,
      user.password
    );

    if (!isPasswordCorrect)
      throw new BookingError(
        'auth.services.js',
        'loginService',
        'Password is wrong',
        error?.stack
      );

    const token = tokenSigning(user._id, user.isAdmin);

    const { password, isAdmin, ...otherDetails } = user._doc;
    return {
      token: token,
      details: { ...otherDetails },
      isAdmin: isAdmin
    };
  } catch (error) {
    throw new BookingError(
      'auth.services.js',
      'loginService',
      error?.message,
      error?.stack
    );
  }
};
