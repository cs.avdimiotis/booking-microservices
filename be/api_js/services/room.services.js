import {
  createRoom,
  updateRoom,
  updateRoomAvailability,
  deleteRoom,
  findRoom,
  findRooms
} from '../dal/room.dal.js';
import { addHotelRooms, deleteHotelRooms } from '../dal/hotel.dal.js';
import { BookingError } from '../common/error.types.js';

export {
  createRoomService,
  updateRoomService,
  updateRoomAvailabilityService,
  deleteRoomService,
  getRoomService,
  getRoomsService
};

const createRoomService = async (
  hotelId,
  title,
  price,
  maxPeople,
  desc,
  roomNumbers
) => {
  try {
    const newRoom = { title, price, maxPeople, desc, roomNumbers };
    await createRoom(newRoom);

    await addHotelRooms(hotelId, savedRoom._id);
    return;
  } catch (error) {
    throw new BookingError(
      'room.services.js',
      'createRoomService',
      error?.message,
      error?.stack
    );
  }
};

const updateRoomService = async (
  id,
  title,
  price,
  maxPeople,
  desc,
  roomNumbers
) => {
  try {
    const updatedRoom = { title, price, maxPeople, desc, roomNumbers };
    await updateRoom(id, updatedRoom);
    return;
  } catch (error) {
    throw new BookingError(
      'room.services.js',
      'updateRoomService',
      error?.message,
      error?.stack
    );
  }
};

const updateRoomAvailabilityService = async (id, dates) => {
  try {
    await updateRoomAvailability(id, dates);
    return;
  } catch (error) {
    throw new BookingError(
      'room.services.js',
      'updateRoomAvailabilityService',
      error?.message,
      error?.stack
    );
  }
};

const deleteRoomService = async (hotelId, id) => {
  try {
    await deleteRoom(id);
    await deleteHotelRooms(hotelId, id);
    return;
  } catch (err) {
    throw new BookingError(
      'room.services.js',
      'deleteRoomService',
      error?.message,
      error?.stack
    );
  }
};

const getRoomService = async (id) => {
  try {
    const room = findRoom(id);
    return room;
  } catch (error) {
    throw new BookingError(
      'room.services.js',
      'getRoomService',
      error?.message,
      error?.stack
    );
  }
};

const getRoomsService = async () => {
  try {
    const rooms = await findRooms();
    return rooms;
  } catch (error) {
    throw new BookingError(
      'room.services.js',
      'getRoomsService',
      error?.message,
      error?.stack
    );
  }
};
