// get the username and password from environment variables
const username = 'bookingUser';
const password = 'bookingUser';
const database = 'booking';
const admin = 'admin';
const collection1 = 'hotels';
const collection2 = 'rooms';
const collection3 = 'users';

console.log(admin);
// create a new database
db = db.getSiblingDB(database);

// create a new user with read and write privileges on the database
db.createUser({
  user: username,
  pwd: password,
  roles: [{ role: 'readWrite', db: database }]
});

db.grantRolesToUser(admin, [{ role: 'readWrite', db: database }]);

// Create collections and insert data as the bookingUser
db.auth(username, password);
db.createCollection(collection1);
db.createCollection(collection2);
db.createCollection(collection3);
