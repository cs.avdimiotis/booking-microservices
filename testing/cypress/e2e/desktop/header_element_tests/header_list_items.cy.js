import cleanUp from "../../../support/helpers/cleanUp";
import headerElement from "../../../support/page_objects/headerElements";
import navigationBar from "../../../support/page_objects/navigationBar";

let previousTestStatus = false;
let cleanUpChecks = {};

describe("Header element tests", () => {
  before(() => {
    cy.visit("/");
  });

  beforeEach(() => {
    if (previousTestStatus) {
      cleanUp.cleanUpTest(cleanUpChecks);
      previousTestStatus = false;
    }
  });

  afterEach(function () {
    if (this.currentTest.state === "failed") {
      previousTestStatus = true;
    }
  });

  //https://testing-booking.atlassian.net/browse/BOOK-9
  it("Header element basic functionality", () => {
    cleanUpChecks = {
      checks: {
        popUpOrDDIsOpen: true,
      },
    };
    headerElement.verifyHeaderContainerIsVisible(true);
    headerElement.verifyHeaderListItemsAreVisible();
    headerElement.clickOnStays();
    cy.url().should("eq", "http://localhost:3000/walletPage");
    navigationBar.clickOnLogo();
    // headerElement.userMenu.verifyManageAccountOptionIsVisible(true);
    // cy.clickOutsideElement();
  });

  it("Dummy test for clean up", () => {
    cy.log("Clean up");
  });
});
