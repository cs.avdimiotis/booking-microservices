const navigationBar = require("../../../../support/page_objects/navigationBar.js");
// const {
//   userMenuOptions
// } = require('../../../../support/page_objects/navigationBar');

//let previousConditionFailed = false;

describe("User menu; Basic tests", () => {
  before(() => {
    cy.visit("/");
  });

  // beforeEach(() => {
  //   if(previousConditionFailed){
  //     //Clean up
  //     navigationBar.clickOnUsername();
  //   }

  // });

  // afterEach(function () {
  //   previousConditionFailed = this.test.isFailed
  // });

  //https://jira.com/1001
  it("Verify username and genius level", () => {
    navigationBar.verifyUsername("Giwta Georgiadou", "2");
  });

  //https://jira.com/1002
  it("Verify that user menu is appearing", () => {
    navigationBar.clickOnUsername();

    //TODO complete this test and put clean up section
  });

  //https://jira.com/1003
  it("User menu option should have option Manage Account and should be clickable", () => {
    navigationBar.clickOnUsername();
    navigationBar.userMenu.verifyManageAccountOptionIsVisible(true);
    navigationBar.userMenu.verifyManageAccountOptionHasText();
    navigationBar.userMenu.verifyManageAccountHasColor();
    navigationBar.userMenu.clickOnManageAccount();
  });

  //https://jira.com/1004 //TODO fix this test
  it.skip("User menu option should have option Booking & Trips", () => {
    navigationBar.clickOnUsername();
    navigationBar.userMenu.clickOnBookingTrips();
  });

  //https://jira.com/1005 //TODO fix this test
  it.skip("User menu option should have option Genius loyalty programme", () => {
    navigationBar.clickOnUsername();
    navigationBar.userMenu.clickOnGeniusLoyaltyProgramme();
  });

  //https://jira.com/1006 //TODO fix this test
  it.skip("User menu option should have option Rewards & Wallet", () => {
    navigationBar.clickOnUsername();
    navigationBar.userMenu.clickOnRewardsAndWallet();
  });

  //https://jira.com/1007 //TODO fix this test
  it.skip("User menu option should have option Reviews", () => {
    navigationBar.clickOnUsername();
    navigationBar.userMenu.clickOnReviews();
  });

  //https://jira.com/1008 //TODO fix this test
  it.skip("User menu option should have option Saved", () => {
    navigationBar.clickOnUsername();
    navigationBar.userMenu.clickOnSaved();
  });

  //https://jira.com/1009 //TODO fix this test
  it.skip("User menu option should have option Sign Out", () => {
    navigationBar.clickOnUsername();
    navigationBar.userMenu.clickOnSignOut();
  });

  /*
  //https://jira.com/1003
  it("User menu option should have option Booking & Trips", () => {
    cy.visit("http://localhost:3000");

    cy.get('[data-cy="user-menu-button"]').should("be.visible");
    navigationBar.clickOnUsername();

    cy.get('[data-cy="menu"]').should("be.visible");

    cy.get('[data-cy="booking-trips-option"]').should("be.visible");
    cy.get('[data-cy="booking-trips-icon"]').should("be.visible");
    cy.get('[data-cy="booking-trips-text"]').contains("Booking & Trips");
  });
  */

  /*
  // Example with commands and selectors files

  it("User menu option should have option Manage Account", () => {
    cy.visit("http://localhost:3000");

    // cy.get(selectors.USER_MENU).should('be.visible');
    navigationBar.verifyUsername("Giwta Georgiadou", "2");
    //navigationBar.userMenu.clickOnSomething();
    //cy.openingUserMenu();

    // cy.get('[data-cy="menu"]').should('be.visible');

    // cy.get('[data-cy="manage-account-option"]').should('be.visible');
    // cy.get('[data-cy="manage-account-icon"]').should('be.visible');
    // cy.get('[data-cy="manage-account-text"]').contains('Manage account');
  });


    it("User menu option should have option Booking & Trips", () => {
    cy.visit("http://localhost:3000");
    cy.get(selectors.USER_MENU).should("be.visible");
    navigationBar.clickOnUsername();

    cy.get('[data-cy="menu"]').should("be.visible");

    cy.get('[data-cy="booking-trips-option"]').should("be.visible");
    cy.get('[data-cy="booking-trips-icon"]').should("be.visible");
    cy.get('[data-cy="booking-trips-text"]').contains("Booking & Trips");
  });
  */
});
