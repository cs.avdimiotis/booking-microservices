import cleanUp from "../../../support/helpers/cleanUp";
import navigationBar from "../../../support/page_objects/navigationBar";

let previousTestStatus = false;
let cleanUpChecks = {};

describe("User menu tests", () => {
  before(() => {
    cy.visit("/");
  });

  beforeEach(() => {
    if (previousTestStatus) {
      cleanUp.cleanUpTest(cleanUpChecks);
      previousTestStatus = false;
    }
  });

  afterEach(function () {
    if (this.currentTest.state === "failed") {
      previousTestStatus = true;
    }
  });

  //https://testing-booking.atlassian.net/browse/BOOK-3
  it("Usermenu basic functionality", () => {
    cleanUpChecks = {
      checks: {
        popUpOrDDIsOpen: true,
      },
    };
    navigationBar.verifyNavigationBarIsVisible(true);
    navigationBar.verifyBasicElements();
    navigationBar.clickOnUsername();
    navigationBar.userMenu.verifyAllOptions();
    cy.clickOutsideElement();
    navigationBar.clickOnGeniusLevel();
    navigationBar.userMenu.verifyAllOptions();

    navigationBar.verifyLocationContainer("Berlin", true);
    navigationBar.verifyLocationContainer("Ohio", false);
    // Clean up
    cy.clickOutsideElement();
  });

  //https://testing-booking.atlassian.net/browse/BOOK-5
  it("User menu option should have option Manage Account and should be clickable", () => {
    cleanUpChecks = {
      checks: {
        popUpOrDDIsOpen: true,
      },
    };
    navigationBar.verifyNavigationBarIsVisible(true);
    navigationBar.clickOnUsername();
    navigationBar.userMenu.verifyManageAccountOptionIsVisible(true);
    navigationBar.userMenu.verifyManageAccountOptionHasText();
    navigationBar.userMenu.verifyManageAccountIconIsVisible(true);
    navigationBar.userMenu.clickOnManageAccount();
    // cy.clickOutsideElement();
    navigationBar.clickOnGeniusLevel();
    navigationBar.userMenu.verifyManageAccountOptionIsVisible(true);
    // Clean up
    cy.clickOutsideElement();
  });

  //https://testing-booking.atlassian.net/browse/BOOK-10
  it("User menu option should have option Booking & Trips and should be clickable", () => {
    cleanUpChecks = {
      checks: {
        popUpOrDDIsOpen: true,
      },
    };
    navigationBar.verifyNavigationBarIsVisible(true);
    navigationBar.clickOnUsername();
    navigationBar.userMenu.verifyBookingAndTripsOptionIsVisible(true);
    navigationBar.userMenu.verifyBookingAndTripsOptionHasText();
    navigationBar.userMenu.verifyBookingAndTripsIconIsVisible(true);
    navigationBar.userMenu.clickOnBookingAndTrips();
    navigationBar.clickOnGeniusLevel();
    navigationBar.userMenu.verifyBookingAndTripsOptionIsVisible(true);
    // Clean up
    cy.clickOutsideElement();
  });

  //https://testing-booking.atlassian.net/browse/BOOK-11
  it("User menu option should have option Genius loyalty programme and should be clickable", () => {
    cleanUpChecks = {
      checks: {
        popUpOrDDIsOpen: true,
      },
    };
    navigationBar.verifyNavigationBarIsVisible(true);
    navigationBar.clickOnUsername();
    navigationBar.userMenu.verifyGeniusLoyaltyProgrammeOptionIsVisible(true);
    navigationBar.userMenu.verifyGeniusLoyaltyProgrammeOptionHasText();
    navigationBar.userMenu.verifyGeniusLoyaltyProgrammeIconIsVisible(true);
    navigationBar.userMenu.clickOnGeniusLoyaltyProgramme();
    navigationBar.clickOnGeniusLevel();
    navigationBar.userMenu.verifyGeniusLoyaltyProgrammeOptionIsVisible(true);
    // Clean up
    cy.clickOutsideElement();
  });

  //https://testing-booking.atlassian.net/browse/BOOK-12
  it("User menu option should have option Rewards & Wallet and should be clickable", () => {
    cleanUpChecks = {
      checks: {
        popUpOrDDIsOpen: true,
      },
    };
    navigationBar.verifyNavigationBarIsVisible(true);
    navigationBar.clickOnUsername();
    navigationBar.userMenu.verifyRewardsAndWalletOptionIsVisible(true);
    navigationBar.userMenu.verifyRewardsAndWalletOptionHasText();
    navigationBar.userMenu.verifyRewardsAndWalletIconIsVisible(true);
    navigationBar.userMenu.clickOnRewardsAndWallet();
    cy.url().should("eq", "http://localhost:3000/walletPage");
    navigationBar.clickOnGeniusLevel();
    navigationBar.userMenu.verifyRewardsAndWalletOptionIsVisible(true);
    //cy.visit("/");
    cy.clickOutsideElement();
    navigationBar.clickOnLogo();

    // Clean up
    cy.clickOutsideElement();
  });

  //https://testing-booking.atlassian.net/browse/BOOK-13
  it("User menu option should have option Reviews and should be clickable", () => {
    cleanUpChecks = {
      checks: {
        popUpOrDDIsOpen: true,
      },
    };
    navigationBar.verifyNavigationBarIsVisible(true);
    navigationBar.clickOnUsername();
    navigationBar.userMenu.verifyReviewsOptionIsVisible(true);
    navigationBar.userMenu.verifyReviewsOptionHasText();
    navigationBar.userMenu.verifyReviewsIconIsVisible(true);
    navigationBar.userMenu.clickOnReviews();
    navigationBar.clickOnGeniusLevel();
    navigationBar.userMenu.verifyReviewsOptionIsVisible(true);

    // Clean up
    cy.clickOutsideElement();
  });

  //https://testing-booking.atlassian.net/browse/BOOK-14
  it("User menu option should have option Saved and should be clickable", () => {
    cleanUpChecks = {
      checks: {
        popUpOrDDIsOpen: true,
      },
    };
    navigationBar.verifyNavigationBarIsVisible(true);
    navigationBar.clickOnUsername();
    navigationBar.userMenu.verifySavedOptionIsVisible(true);
    navigationBar.userMenu.verifySavedOptionHasText();
    navigationBar.userMenu.verifySavedIconIsVisible(true);
    navigationBar.userMenu.clickOnSaved();
    navigationBar.clickOnGeniusLevel();
    navigationBar.userMenu.verifySavedOptionIsVisible(true);

    // Clean up
    cy.clickOutsideElement();
  });

  //https://testing-booking.atlassian.net/browse/BOOK-15
  it("User menu option should have option Sign out and should be clickable", () => {
    cleanUpChecks = {
      checks: {
        popUpOrDDIsOpen: true,
      },
    };
    navigationBar.verifyNavigationBarIsVisible(true);
    navigationBar.clickOnUsername();
    navigationBar.userMenu.verifySignOutOptionIsVisible(true);
    navigationBar.userMenu.verifySignOutOptionHasText();
    navigationBar.userMenu.verifySignOutIconIsVisible(true);
    navigationBar.userMenu.clickOnSignOut();
    navigationBar.clickOnGeniusLevel();
    navigationBar.userMenu.verifySignOutOptionIsVisible(true);

    // Clean up
    cy.clickOutsideElement();
  });

  it("Dummy test for clean up", () => {
    cy.log("Clean up");
  });
});
