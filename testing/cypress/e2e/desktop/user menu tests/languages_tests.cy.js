import cleanUp from "../../../support/helpers/cleanUp";
import navigationBar from "../../../support/page_objects/navigationBar";

let previousTestStatus = false;
let cleanUpChecks = {};

describe("User menu tests", () => {
  before(() => {
    cy.visit("/");
  });

  beforeEach(() => {
    if (previousTestStatus) {
      cleanUp.cleanUpTest(cleanUpChecks);
      previousTestStatus = false;
    }
  });

  afterEach(function () {
    if (this.currentTest.state === "failed") {
      previousTestStatus = true;
    }
  });

  //https://testing-booking.atlassian.net/browse/BOOK-7
  it("Languages option should be clickable and appear a popup menu with the word Languages", () => {
    cleanUpChecks = {
      checks: {
        popUpOrDDIsOpen: true,
      },
    };
    navigationBar.verifyNavigationBarIsVisible(true);
    navigationBar.verifyLanguagesButton();
    navigationBar.clickOnLanguages();
    navigationBar.verifyLanguagesText();
    navigationBar.verifyCloseLanguagesButton();
    navigationBar.clickOnCloseLanguageButton();
    navigationBar.clickOnLanguages();

    // Clean up
    cy.clickOutsideElement();
  });

  it("Dummy test for clean up", () => {
    cy.log("Clean up");
  });
});
