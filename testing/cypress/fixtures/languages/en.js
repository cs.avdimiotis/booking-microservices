module.exports = {
  userlevel: "Genius Level",
  Manage_account: "Manage account",
  Booking_trips: "Booking & Trips",
  Genius_loyalty_programme: "Genius loyalty programme",
  Rewards_wallet: "Rewards & Wallet",
  Reviews: "Reviews",
  Saved: "Saved",
  Sign_out: "Sign out",
};
