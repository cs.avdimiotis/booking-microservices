module.exports = {
  baseUrl: 'http://127.0.0.1:3000',
  env: {
    USERNAME: 'bob',
    JAVA_BE: 'http://localhost:8801/api/v1/',
    JS_BE: 'http://localhost:8800/api/'
  }
};
