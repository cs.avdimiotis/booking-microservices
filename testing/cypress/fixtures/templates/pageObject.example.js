const texts = Cypress.env('translationFile');

// Selectors
const selectors = {
  CONSTANT_SELECTOR: '[data-cy="a-selector-of-a-button"]',
  VARIATED_SELECTOR: '[data-cy="a-selector-with-{a-variable-part}"]',
  PREFIX_SELECTOR: 'a-prefix'
};

export const enumer = {
  //The options of a menu
};

class sub_class {
  // A menu, a drop-down menu etc --> Should exist only in this page_object
}

// The main class  (can inherit some default methods if it is i.e. a pop up window or a collapsed panel)
class headerElement extends Some_other_class {
  //Instatiation of a sub-page-object
  steady_sub_object = new sub_class();

  //Instatiation of a more than one sub page object
  more_than_one_instances_1 = new another_sub_class(PREFIX_SELECTOR);
  more_than_one_instances_2 = new another_sub_class(PREFIX_SELECTOR_2);

  //Section of methods that do something
  clickOnSomething() {
    cy.get(selectors.CONSTANT_SELECTOR).should('be.visible').click();
  }

  typeOnSomething(replaceID, someMessage) {
    const selector = selectors.VARIATED_SELECTOR.replace(
      '{a-variable-part}',
      replaceID
    );
    cy.get(selector).type(someMessage);
  }

  //Section of methods that verify something (always start with the verify word)
  verifyHeaderContainerIsVisible(shouldBeVisible) {
    if (shouldBeVisible) {
      cy.get(selectors.PARENT_SELECTOR + ' ' + selectors.CHILD_SELECTOR).should(
        'be.visible'
      );
    } else {
      cy.get(selectors.PARENT_SELECTOR + ' ' + selectors.CHILD_SELECTOR).should(
        'not.be.visible'
      );
    }
  }
}

export default new headerElement();
