const texts = require("../../fixtures/languages/en");

const selectors = {
  //Header element
  HEADER_CONTAINER: '[data-cy="header-container"]',
  STAYS: '[data-cy="list-item-stays"]',
  STAYS_ICON: '[data-cy="list-item-stays-icon"]',
  FLIGHTS: '[data-cy="list-item-flights"]',
  FLIGHTS_ICON: '[data-cy="list-item-flights-icon"]',
  CAR_RENTAL: '[data-cy="list-item-car-rentals"]',
  CAR_RENTAL_ICON: '[data-cy="list-item-car-rentals-icon"]',
  ATTRACTIONS: '[data-cy="list-item-attractions"]',
  ATTRACTIONS_ICON: '[data-cy="list-item-attractions-icon"]',
  AIRPORT_TAXIS: '[data-cy="list-item-airport-taxis"]',
  AIRPORT_TAXIS_ICON: '[data-cy="list-item-airport-taxis-icon"]',
};

class headerElement {
  //Click
  clickOnStays() {
    cy.get(selectors.STAYS).should("be.visible").click();
  }

  /**
   * Clicks on logo
   * @param none
   */

  verifyHeaderContainerIsVisible(shouldBeVisible) {
    if (shouldBeVisible) {
      cy.get(selectors.HEADER_CONTAINER)
        .should("be.visible")
        .should("have.css", "width", "1483px")
        .should("have.css", "justify-content", "center");
    } else {
      cy.get(selectors.CONTAINER).should("not.be.visible");
    }
  }

  verifyHeaderListItemsAreVisible() {
    cy.get(selectors.HEADER_CONTAINER + " " + selectors.STAYS).should(
      "be.visible"
    );
    cy.get(selectors.HEADER_CONTAINER + " " + selectors.STAYS_ICON).should(
      "be.visible"
    );
    cy.get(selectors.HEADER_CONTAINER + " " + selectors.FLIGHTS).should(
      "be.visible"
    );
    cy.get(selectors.HEADER_CONTAINER + " " + selectors.FLIGHTS_ICON).should(
      "be.visible"
    );
    cy.get(selectors.HEADER_CONTAINER + " " + selectors.CAR_RENTAL).should(
      "be.visible"
    );
    cy.get(selectors.HEADER_CONTAINER + " " + selectors.ATTRACTIONS).should(
      "be.visible"
    );
    cy.get(
      selectors.HEADER_CONTAINER + " " + selectors.ATTRACTIONS_ICON
    ).should("be.visible");
    cy.get(selectors.HEADER_CONTAINER + " " + selectors.AIRPORT_TAXIS).should(
      "be.visible"
    );
    cy.get(
      selectors.HEADER_CONTAINER + " " + selectors.AIRPORT_TAXIS_ICON
    ).should("be.visible");
  }
}

export default new headerElement();
