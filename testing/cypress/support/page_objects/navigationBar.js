// Dynamic Selectors
// Sub page objects
// Helpers
// Aliases
// Interceptors

const texts = require("../../fixtures/languages/en");

const selectors = {
  //navBar selectors
  CONTAINER: '[data-cy="navigation-bar"]',
  LOGO: '[data-cy="logo"]',
  REGISTER_BUTTON: '[data-cy="register-button"]',
  LOGIN_BUTTON: '[data-cy="login-button"]',
  LANGUAGES_BUTTON: '[data-cy="languagesection-languages-icon"]',
  HELP_ICON: '[data-cy="help-icon"]',
  USER_MENU: '[data-cy="user-menu-button"]',
  USERNAME_USERNAME: '[data-cy="user-menu-username"]',
  USERNAME_LEVEL: '[data-cy="user-menu-userlevel"]',

  //userMenu selectors
  USERMENU_CONTAINER: "ul.MuiList-root",
  MANAGE_ACCOUNT: '[data-cy="manage-account-option"]',
  MANAGE_ACCOUNT_ICON: '[data-cy="manage-account-icon"]',
  BOOKING_TRIPS: '[data-cy="booking-trips-option"]',
  BOOKING_TRIPS_ICON: '[data-cy="booking-trips-icon"]',
  GENIUS_LOYALTY_PROGRAMME: '[data-cy="genius-loyalty-programme"]',
  GENIUS_LOYALTY_PROGRAMME_ICON: '[data-cy="genius-loyalty-programme-icon"]',
  REWARDS_WALLET: '[data-cy="rewards-and-wallet"]',
  REWARDS_WALLET_ICON: '[data-cy="rewards-and-wallet-icon"]',
  REVIEWS: '[data-cy="reviews"]',
  REVIEWS_ICON: '[data-cy="reviews-icon"]',
  SAVED: '[data-cy="saved-options"]',
  SAVED_ICON: '[data-cy="saved-options-icon"]',
  SIGN_OUT: '[data-cy="sign-out"]',
  SIGN_OUT_ICON: '[data-cy="sign-out-icon"]',

  //languages selectors
  LOCATIONS_CONTAINER: "[]",
  LANGUAGES_CLOSE: '[data-cy="languages-close-button"]',
  LANGUAGES_TEXT: '[data-cy="languages-text"]',

  //locations
  LOCATIONS_CONTAINER: '[data-cy="container-{name_of_location}"]',
};

const geniusColors = {
  gold: "rgb(255, 215, 0)",
  grey: "rgba(0, 0, 0, 0.87)",
};

// export const userMenuOptions = {
//   //Enumerator Enum --> Typescript
//   MANAGE_ACCOUNT: 1,
//   BOOKING_TRIPS: 5,
//   GENIUS_LOYALTY_PROGRAMME: 3
// };

// const userMenuConnector = {
//   [userMenuOptions.MANAGE_ACCOUNT] : {
//     textSelector: selectors.MANAGE_ACCOUNT,
//     iconSelector: selectors.MANAGE_ACCOUNT_ICON
//   },
//   [userMenuOptions.BOOKING_TRIPS] : {
//     textSelector: selectors.BOOKING_TRIPS
//   },
//   [userMenuOptions.GENIUS_LOYALTY_PROGRAMME] : selectors.GENIUS_LOYALTY_PROGRAMME
// }

//Object
// const name = 'tomatoes'
// const quantity = 2

// const fridge = {
//   [name]: quantity
// }

// const fridge2 = {
//   tomatoes: { quantity: 2, value: 2.99}
// }
// fridge2.tomatoes
// fridge2['tomatoes']

class userMenu {
  // clickOnOption(selectedOption) {
  //   let selector = userMenuConnector[selectedOption].textSelector;
  //   cy.get(selector).should('be.visible').click();
  // }

  //Manage Account
  clickOnManageAccount() {
    cy.get(selectors.MANAGE_ACCOUNT).should("be.visible").click();
  }

  verifyManageAccountOptionIsVisible(shouldBeVisible) {
    if (shouldBeVisible) {
      cy.get(selectors.MANAGE_ACCOUNT).should("be.visible");
    } else {
      cy.get(selectors.MANAGE_ACCOUNT).should("not.be.visible");
    }
  }

  verifyManageAccountIconIsVisible(shouldBeVisible) {
    if (shouldBeVisible) {
      cy.get(selectors.MANAGE_ACCOUNT_ICON).should("be.visible");
    } else {
      cy.get(selectors.MANAGE_ACCOUNT_ICON).should("not.be.visible");
    }
  }

  verifyManageAccountOptionHasText() {
    cy.get(selectors.MANAGE_ACCOUNT)
      .should("be.visible")
      .contains(texts.Manage_account);
  }

  verifyManageAccountHasColor() {
    cy.get(selectors.MANAGE_ACCOUNT)
      .should("be.visible")
      .should("have.css", "color", geniusColors.grey);
  }

  //Booking & Trips
  clickOnBookingAndTrips() {
    cy.get(selectors.BOOKING_TRIPS).should("be.visible").click();
  }

  verifyBookingAndTripsOptionIsVisible(shouldBeVisible) {
    if (shouldBeVisible) {
      cy.get(selectors.BOOKING_TRIPS).should("be.visible");
    } else {
      cy.get(selectors.BOOKING_TRIPS).should("not.be.visible");
    }
  }

  verifyBookingAndTripsIconIsVisible(shouldBeVisible) {
    if (shouldBeVisible) {
      cy.get(selectors.BOOKING_TRIPS_ICON).should("be.visible");
    } else {
      cy.get(selectors.BOOKING_TRIPS_ICON).should("not.be.visible");
    }
  }

  verifyBookingAndTripsOptionHasText() {
    cy.get(selectors.BOOKING_TRIPS)
      .should("be.visible")
      .contains(texts.Booking_trips);
  }

  //Genius loyalty programme
  clickOnGeniusLoyaltyProgramme() {
    cy.get(selectors.GENIUS_LOYALTY_PROGRAMME).should("be.visible").click();
  }

  verifyGeniusLoyaltyProgrammeOptionIsVisible(shouldBeVisible) {
    if (shouldBeVisible) {
      cy.get(selectors.GENIUS_LOYALTY_PROGRAMME).should("be.visible");
    } else {
      cy.get(selectors.GENIUS_LOYALTY_PROGRAMME).should("not.be.visible");
    }
  }

  verifyGeniusLoyaltyProgrammeIconIsVisible(shouldBeVisible) {
    if (shouldBeVisible) {
      cy.get(selectors.GENIUS_LOYALTY_PROGRAMME_ICON).should("be.visible");
    } else {
      cy.get(selectors.GENIUS_LOYALTY_PROGRAMME_ICON).should("not.be.visible");
    }
  }

  verifyGeniusLoyaltyProgrammeOptionHasText() {
    cy.get(selectors.GENIUS_LOYALTY_PROGRAMME)
      .should("be.visible")
      .contains(texts.Genius_loyalty_programme);
  }

  //Rewards & Wallet
  clickOnRewardsAndWallet() {
    cy.get(selectors.REWARDS_WALLET).should("be.visible").click();
  }

  verifyRewardsAndWalletOptionIsVisible(shouldBeVisible) {
    if (shouldBeVisible) {
      cy.get(selectors.REWARDS_WALLET).should("be.visible");
    } else {
      cy.get(selectors.REWARDS_WALLET).should("not.be.visible");
    }
  }

  verifyRewardsAndWalletIconIsVisible(shouldBeVisible) {
    if (shouldBeVisible) {
      cy.get(selectors.REWARDS_WALLET_ICON).should("be.visible");
    } else {
      cy.get(selectors.REWARDS_WALLET_ICON).should("not.be.visible");
    }
  }

  verifyRewardsAndWalletOptionHasText() {
    cy.get(selectors.REWARDS_WALLET)
      .should("be.visible")
      .contains(texts.Rewards_wallet);
  }

  //Reviews
  clickOnReviews() {
    cy.get(selectors.REVIEWS).should("be.visible").click();
  }

  verifyReviewsOptionIsVisible(shouldBeVisible) {
    if (shouldBeVisible) {
      cy.get(selectors.REVIEWS).should("be.visible");
    } else {
      cy.get(selectors.REVIEWS).should("not.be.visible");
    }
  }

  verifyReviewsIconIsVisible(shouldBeVisible) {
    if (shouldBeVisible) {
      cy.get(selectors.REVIEWS_ICON).should("be.visible");
    } else {
      cy.get(selectors.REVIEWS_ICON).should("not.be.visible");
    }
  }

  verifyReviewsOptionHasText() {
    cy.get(selectors.REVIEWS).should("be.visible").contains(texts.Reviews);
  }

  //Saved
  clickOnSaved() {
    cy.get(selectors.SAVED).should("be.visible").click();
  }

  verifySavedOptionIsVisible(shouldBeVisible) {
    if (shouldBeVisible) {
      cy.get(selectors.SAVED).should("be.visible");
    } else {
      cy.get(selectors.SAVED).should("not.be.visible");
    }
  }

  verifySavedIconIsVisible(shouldBeVisible) {
    if (shouldBeVisible) {
      cy.get(selectors.SAVED_ICON).should("be.visible");
    } else {
      cy.get(selectors.SAVED_ICON).should("not.be.visible");
    }
  }

  verifySavedOptionHasText() {
    cy.get(selectors.SAVED).should("be.visible").contains(texts.Saved);
  }

  //Sign Out
  clickOnSignOut() {
    cy.get(selectors.SIGN_OUT).should("be.visible").click();
  }

  verifySignOutOptionIsVisible(shouldBeVisible) {
    if (shouldBeVisible) {
      cy.get(selectors.SIGN_OUT).should("be.visible");
    } else {
      cy.get(selectors.SIGN_OUT).should("not.be.visible");
    }
  }

  verifySignOutIconIsVisible(shouldBeVisible) {
    if (shouldBeVisible) {
      cy.get(selectors.SIGN_OUT_ICON).should("be.visible");
    } else {
      cy.get(selectors.SIGN_OUT_ICON).should("not.be.visible");
    }
  }

  verifySignOutOptionHasText() {
    cy.get(selectors.SIGN_OUT).should("be.visible").contains(texts.Sign_out);
  }

  //All options

  verifyAllOptions() {
    cy.get(selectors.USERMENU_CONTAINER + " " + selectors.MANAGE_ACCOUNT);
    cy.get(selectors.USERMENU_CONTAINER + " " + selectors.MANAGE_ACCOUNT_ICON);
    cy.get(selectors.USERMENU_CONTAINER + " " + selectors.BOOKING_TRIPS);
    cy.get(selectors.USERMENU_CONTAINER + " " + selectors.BOOKING_TRIPS_ICON);
    cy.get(selectors.USERMENU_CONTAINER + " " + selectors.REWARDS_WALLET);
    cy.get(selectors.USERMENU_CONTAINER + " " + selectors.REWARDS_WALLET_ICON);
    cy.get(
      selectors.USERMENU_CONTAINER + " " + selectors.GENIUS_LOYALTY_PROGRAMME
    );
    cy.get(
      selectors.USERMENU_CONTAINER +
        " " +
        selectors.GENIUS_LOYALTY_PROGRAMME_ICON
    );
    cy.get(selectors.USERMENU_CONTAINER + " " + selectors.REVIEWS);
    cy.get(selectors.USERMENU_CONTAINER + " " + selectors.REVIEWS_ICON);
    cy.get(selectors.USERMENU_CONTAINER + " " + selectors.SAVED);
    cy.get(selectors.USERMENU_CONTAINER + " " + selectors.SAVED_ICON);
    cy.get(selectors.USERMENU_CONTAINER + " " + selectors.SIGN_OUT);
    cy.get(selectors.USERMENU_CONTAINER + " " + selectors.SIGN_OUT_ICON);
  }

  // clickOnBookingTrips() {
  //   cy.get('[data-cy="booking-trips-icon"]').should('be.visible');
  //   cy.get(selectors.BOOKING_TRIPS)
  //     .should('be.visible')
  //     .contains(texts.Booking_trips)
  //     .click();
  // }

  // clickOnGeniusLoyaltyProgramme() {
  //   cy.get('[data-cy="genius-loyalty-programme"]').should('be.visible');
  //   cy.get(selectors.GENIUS_LOYALTY_PROGRAMME)
  //     .should('be.visible')
  //     .contains(texts.Genius_loyalty_programme)
  //     .click();
  // }

  // clickOnRewardsAndWallet() {
  //   cy.get('[data-cy="rewards-and-wallet"]').should('be.visible');
  //   cy.get(selectors.REWARDS_WALLET)
  //     .should('be.visible')
  //     .contains(texts.Rewards_wallet)
  //     .click();
  // }

  // clickOnReviews() {
  //   cy.get('[data-cy="reviews"]').should('be.visible');
  //   cy.get(selectors.REVIEWS)
  //     .should('be.visible')
  //     .contains(texts.Reviews)
  //     .click();
  // }

  // clickOnSaved() {
  //   cy.get('[data-cy="saved-options"]').should('be.visible');
  //   cy.get(selectors.SAVED).should('be.visible').contains(texts.Saved).click();
  // }

  // clickOnSignOut() {
  //   cy.get('[data-cy="sign-out"]').should('be.visible');
  //   cy.get(selectors.SIGN_OUT)
  //     .should('be.visible')
  //     .contains(texts.Sign_out)
  //     .click();
  // }

  // verifySignOutIsNotVisible() {
  //   cy.get('[data-cy="sign-out"]').should('not.be.visible');
  // }

  // verifySignOutIsAtSomePointVisible() {
  //   cy.get('[data-cy="sign-out"]').should('not.be.visible');
  //   cy.get('[data-cy="sign-out"]').should('be.visible');
  //   cy.get(selectors.SIGN_OUT)
  //     .should('be.visible')
  //     .contains(texts.Sign_out)
  //     .click();
  // }
}

class navigationBar {
  userMenu = new userMenu();

  /**
   * Clicks on logo
   * @param none
   */
  clickOnLogo() {
    // Command chaining
    cy.get(selectors.LOGO).should("be.visible").click();
  }

  clickOnUsername() {
    cy.get(selectors.USERNAME_USERNAME).should("be.visible").click();
  }

  clickOnGeniusLevel() {
    cy.get(selectors.USERNAME_LEVEL).should("be.visible").click();
  }

  clickOnLanguages() {
    cy.get(selectors.LANGUAGES_BUTTON).should("be.visible").click();
  }

  clickOnCloseLanguageButton() {
    cy.get(selectors.LANGUAGES_CLOSE).should("be.visible").click();
  }

  /**
   * Verifies nav bar is visible
   * @param {Boolean} shouldBeVisible Checks visibility
   */

  verifyBasicElements() {
    cy.get(selectors.CONTAINER + " " + selectors.LOGO).should("be.visible");
    cy.get(selectors.CONTAINER + " " + selectors.REGISTER_BUTTON).should(
      "be.visible"
    );
    cy.get(selectors.CONTAINER + " " + selectors.LOGIN_BUTTON).should(
      "be.visible"
    );
    cy.get(selectors.CONTAINER + " " + selectors.LANGUAGES_BUTTON).should(
      "be.visible"
    );
    cy.get(selectors.CONTAINER + " " + selectors.HELP_ICON).should(
      "be.visible"
    );
    cy.get(selectors.CONTAINER + " " + selectors.USERNAME_USERNAME).should(
      "be.visible"
    );
    cy.get(selectors.CONTAINER + " " + selectors.USERNAME_LEVEL).should(
      "be.visible"
    );
  }

  verifyCloseLanguagesButton() {
    cy.get(selectors.LANGUAGES_CLOSE).should("be.visible");
  }

  verifyLanguagesButton() {
    cy.get(selectors.CONTAINER + " " + selectors.LANGUAGES_BUTTON).should(
      "be.visible"
    );
  }

  verifyLanguagesText() {
    cy.get(selectors.LANGUAGES_TEXT).should("be.visible");
  }

  verifyLocationContainer(nameOfCity, shouldBeVisible) {
    const selector = selectors.LOCATIONS_CONTAINER.replace(
      "{name_of_location}",
      nameOfCity
    );
    if (shouldBeVisible) {
      cy.get(selector).should("be.visible");
    } else {
      cy.get(selector).should("not.exist");
    }
  }

  verifyLogoColor() {
    cy.get(selectors.LOGO).should("have.css", "color", "rgb( , , )");
  }

  verifyLogoIsVisible() {
    cy.get(selectors.LOGO).should("be.visible");
  }

  verifyLogoText() {
    cy.get(selectors.LOGO).contains("GiotBooking");
  }

  verifyNavigationBarIsVisible(shouldBeVisible) {
    if (shouldBeVisible) {
      cy.get(selectors.CONTAINER)
        .should("be.visible")
        .should("have.css", "width", "1483px")
        .should("have.css", "justify-content", "center");
    } else {
      cy.get(selectors.CONTAINER).should("not.be.visible");
    }
  }

  verifyUsername(username, geniusLevel) {
    //Correct way
    cy.get(selectors.USER_MENU + " " + selectors.USERNAME_USERNAME)
      .should("be.visible")
      .contains(username)
      .should("have.css", "color", "rgb(255, 255, 255)");
    cy.get(selectors.USERNAME_LEVEL)
      .should("be.visible")
      .contains(texts.userlevel + " " + geniusLevel)
      .should("have.css", "color", geniusColors.gold);

    //With find
    // cy.get(selectors.USERNAME_MENU)
    //   .find(selectors.USERNAME_USERNAME)
    //   .should('be.visible')
    //   .contains('Giwta Georgiadou')
    //   .should('have.css', 'color', 'rgb(255, 255, 255)');

    //With within
    // cy.get(selectors.USERNAME_MENU).within(() => {
    //   cy.get(selectors.USERNAME_USERNAME)
    //     .should('be.visible')
    //     .contains('Giwta Georgiadou')
    //     .should('have.css', 'color', 'rgb(255, 255, 255)');
    //   cy.get(selectors.USERNAME_LEVEL)
    //     .should('be.visible')
    //     .contains('Genius Level 2')
    //     .should('have.css', 'color', 'rgb(255, 215, 0)');
    // });
  }

  // verifyLogoIsVisible(shouldBeVisible) {
  //   if (shouldBeVisible) {
  //     cy.get(selector.LOGO).should('be.visible');
  //   } else {
  //     cy.get(selector.LOGO).should('not.exist');
  //   }
  // }
}

export default new navigationBar();
