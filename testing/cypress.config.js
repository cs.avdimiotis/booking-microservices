const { defineConfig } = require('cypress');

module.exports = defineConfig({
  viewportHeight: 907,
  viewportWidth: 1500,
  retries: {
    runMode: 2,
    openMode: 1
  },
  e2e: {
    testIsolation: false,
    excludeSpecPattern: ['cypress/e2e/examples/**/**.cy.js'],
    setupNodeEvents(on, config) {
      const environment = config.env.environment;
      const environmentFile = require(`./cypress/fixtures/config/environments/${environment}.environment`);

      config.baseUrl = environmentFile.baseUrl;

      config.env = {
        ...environmentFile.env,
        environmentFile: environmentFile
      };

      return config;
    }
  }
});
