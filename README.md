# Booking Microservices

### Description

This is a POC project to familiarize and develop better techniques on multiple technologies (Full Stack).
It's (supposed to) represent a clone of Booking.com

### Architecture

![Architecture of Booking Microservices](/readme/microservices.png 'Architecture')

### Backend

## API JS

# Technologies & libraries

- Code: Express.js,
- Database: MongoDB,
- Logging: Winston,
- Authentication & Authorization: JWT,
- Hashing: bcrypt
- Testing: Postman (Manually), Jest (Automation) (TBD)
- Documentation: Swagger (TBD)
- Versioning: Remodeled in module structure (TBD)
- Data validation: Express-validator (TBD)
- Caching: Redis or memcached (TBD)
- Security: helmet, etc (TBD)
- Virtualization: Docker

# Architecture (Routes-Controllers-Services-Model)

![Architecture of API JS](/readme/api_js_architecture.png 'Architecture')

# EndPoints

![Endpoints of API JS](/readme/api_js_endpoints.png 'Endpoints')

### FrontEnd

## REACT JS

# Technologies & libraries

- Design: Figma (TBD)
- Code: React.js, Material-UI, Bootstrap,
- Communication: react-query,
- Bundler: Webpack & Babel (TBD),
- Logging: Custom configuration (TBD),
- Authentication & Authorization: JWT,
- Testing: Browser (Manually), Cypress (Automation), Jest (Automation)
- Documentation: React Styleguidist (TBD)
- Versioning: Module structure
- Error-prone: react-query
- Virtualization: Docker

# Architecture (Components - Modules)

![Basic React Structure](/readme/react_basic_structure.png 'Basic Structure')

![Modules structure](/readme/react_all_modules.png 'Modules structure')

![Structure of one module](/readme/react_one_module.png 'Structure of one module')

### Testing

## Cypress

# Technologies & libraries

- Code: Cypress.js,
- Logging: JUnit or Mocha (TBD),
- Versioning: Page object structure
- Tags: Cypress-tags (TBD)
- Documentation: React Styleguidist (TBD)
- Virtualization: Docker

<!-- ![Build Version](https://img.shields.io/badge/Build%20Version-v1.1alpha-red.svg?style=for-the-badge)

![Node Version](https://img.shields.io/badge/node.js-v14.14.0-339933?style=for-the-badge&logo=node.js)

![React Version](https://img.shields.io/badge/react.js-v17.0.1-61DAF8?style=for-the-badge&logo=react) -->

<!-- Visit the current deployed version [here](https://evening-fortress-20708.herokuapp.com/)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

## Prerequisites

1. You need to install [node.js](https://nodejs.org/en/) 14.14.0 or a later LTS release which comes with npm which comes with npm 6.14.4 or later.

2. You need to go to the [OpenWeather API](https://openweathermap.org/), register and request an API Key. Create an .env file and insert the following environmental variable:

```javascript
REACT_APP_WEATHER_API_KEY=/* Your_API_key */
```

3. You need to activate the Google Places API & Google Geolocation API. Insert the Google API key in the following environmental variable:

```javascript
REACT_APP_GOOGLE_API_KEY=/* Your_Google_API_key */
```

4. You need an Google Analytics tracking code ID which should be included in the following environmental variable:

```javascript
REACT_APP_GA_TRACKING_CODE=/* Your_Google_Analytics_Tracking_Code */
```

5. You need an Google Search Console which should be included in the following environmental variable:

```javascript
REACT_APP_GOOGLE_ORGANIC_RANK_ID=/* Your_Google_ownership_verification_code */
```

## Installing

Open the project in your editor and run

```
$ npm install
```

## Deployment

To run the development version ( localhost:3000 ):

```
$ npm start
``` -->

<!-- # Dependencies

- "bootstrap": "^4.6.0",
- "dotenv": "^8.2.0",
- "react": "^17.0.1",
- "react-dom": "^17.0.1",
- "react-scripts": "4.0.3",
- "styled-components": "^5.2.1",
- "web-vitals": "^1.0.1" -->

<!-- ## API -->

<!-- This project is using the following APIs:

- [OpenWeather API](https://openweathermap.org/)
- [Google Places API](https://developers.google.com/maps/documentation/places/web-service/overview)
- [Google Geolocation API](https://developers.google.com/maps/documentation/geolocation/overview) -->

## TODO List

- [x] Add & Containerize React.
- [x] Add & Containerize Angular.
- [x] Add & Containerize Vue.
- [x] Containerize Cypress.
- [x] Add & Containerize Express Javascript.
- [ ] Add & Containerize Express Typescript.
- [ ] Add & Containerize Spring Boot.
- [ ] Containerize PostgresSQL.
- [ ] Containerize MongoDB.
- [ ] Add REDIS.
- [ ] Add Keycloak as authentication tool.
- [ ] Complete README.
